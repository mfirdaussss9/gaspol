<?php

namespace App\Exports;

use App\Monpen;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MonpenExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Monpen::all();
    }   
    public function headings(): array
    {
        return [
            'ID',
            'PENYULANG',
            'UP3',
            'ULP',
            'TGL PADAM',
            'JAM PADAM',
            'TGL NYALA',
            'JAM NYALA',
            'DURASI',
            'BEBAN',
            'ENS',
            'JENIS PMT',
            'SECTION',
            'INDIKASI',
            'R',
            'S',
            'T',
            'N',
            'PENYEBAB',
            'KODE FGTM',
            'VENDOR',
            'JSA',
            'PENGAWAS',
            'CUACA',
            'OPERATOR',
            'CREATED_AT',
            'UPDATED_AT'
        ];
    }
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],

            // Styling a specific cell by coordinate.
            //'B2' => ['font' => ['italic' => true]],

            // Styling an entire column.
            //'C'  => ['font' => ['size' => 16]],
        ];
    }
    
}

<?php

namespace App\Exports;

use App\Monpen;
use Maatwebsite\Excel\Concerns\FromCollection;

class MonpenExport_BLT implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Monpen::Where('UP3','UP3 BELITUNG')->get();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monpen extends Model
{
    protected $table = "monpen";
    protected $fillable = ['penyulang', 'UP3', 'ULP', 'tglpadam', 'jampadam', 'tglnyala', 'jamnyala', 'durasi', 'beban', 'ENS', 'jenis_PMT', 'section', 'indikasi', 'R', 'S', 'T', 'N', 'penyebab', 'kodefgtm', 'vendor', 'jsa', 'pengawas', 'cuaca', 'operator'];

}

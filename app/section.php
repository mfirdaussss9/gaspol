<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class section extends Model
{
    protected $table ="section";
    protected $fillable = ['id','idsectionname','sectionname','jenispmt','penyulang_id','ulp_id'];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\MonpenExport;
use App\Exports\MonpenExport_BLT;
use App\Exports\MonpenExport_BGK;
use Maatwebsite\Excel\Facades\Excel;
use App\monpen;
use Illuminate\Support\Facades\Storage;
class MonpenController extends Controller
{
    public function dashboard(Request $request)
    {
        $tahun = $request->tahun;
        
        $from = $request->from;
        $to = $request->to;
        $ulp = \App\ulp::orderBy('No','asc')->pluck('ulpname');

//DASHBOARD UIW//////////////////////////////////////////////////////////////////////////////////////////////////////////////       
        $pkp_p = \App\monpen::where('ULP', $ulp[0])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $slt_p = \App\monpen::where('ULP', $ulp[1])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $mtk_p = \App\monpen::where('ULP', $ulp[2])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $tbl_p = \App\monpen::where('ULP', $ulp[3])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $kba_p = \App\monpen::where('ULP', $ulp[4])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $pkp_s = \App\monpen::where('ULP', $ulp[0])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $slt_s = \App\monpen::where('ULP', $ulp[1])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $mtk_s = \App\monpen::where('ULP', $ulp[2])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $tbl_s = \App\monpen::where('ULP', $ulp[3])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $kba_s = \App\monpen::where('ULP', $ulp[4])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        
        $tjp_p = \App\monpen::where('ULP', $ulp[5])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $tjp_s = \App\monpen::where('ULP', $ulp[5])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        
        $mgr_p = \App\monpen::where('ULP', $ulp[6])->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $mgr_s = \App\monpen::where('ULP', $ulp[6])->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        
        $bgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $blt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $bgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $blt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $bbl_p = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $bbl_s = \App\monpen::whereBetween('tglpadam',[$from,$to])->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();



//DASHBOARD UP3//////////////////////////////////////////////////////////////////////////////////////////////////////////////       
//GABUNGAN//////////////////////////////////////////////////////////////////////////////////////////////////////////////       
//UIW//////////////////////////////////////////////////////////////////////////////////////////////////////////////       
$janbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$febbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$marbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$aprbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$meibbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$junbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$julbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$agsbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$sepbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$oktbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$novbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$desbbl_p = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$janbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$febbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$marbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$aprbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$meibbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$junbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$julbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$agsbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$sepbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$oktbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$novbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
$desbbl_s = \App\monpen::whereIn('UP3', ['UP3 BANGKA','UP3 BELITUNG'])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

//UP3 BANGKA//////////////////////////////////////////////////////////////////////////////////////////////////////////////       
//PMT///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $janpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jantbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jankba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $febpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febtbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $marpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $martbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $markba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $aprpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprtbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $meipkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meislt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meimtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meitbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meikba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $junpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $juntbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $julpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jultbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $agspkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agstbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agskba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $seppkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $septbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $okttbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $novpkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novtbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novkba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $despkp_p = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desslt_p = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desmtk_p = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $destbl_p = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $deskba_p = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
//SECTION///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $janpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jantbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jankba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febtbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $martbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $markba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprtbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meipkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meislt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meimtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meitbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meikba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $juntbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jultbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agspkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agstbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agskba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $seppkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $septbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $okttbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novpkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novtbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novkba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $despkp_s = \App\monpen::where('ULP', $ulp[0])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desslt_s = \App\monpen::where('ULP', $ulp[1])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desmtk_s = \App\monpen::where('ULP', $ulp[2])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $destbl_s = \App\monpen::where('ULP', $ulp[3])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $deskba_s = \App\monpen::where('ULP', $ulp[4])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
 
        $janbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meibgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desbgk_p = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
       
        $janbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meibgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desbgk_s = \App\monpen::where('UP3', 'UP3 BANGKA')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();


        //UP3 BELITUNG//////////////////////////////////////////////////////////////////////////////////////////////////////////////       
        //PMT///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $jantjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febtjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $martjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprtjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meitjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meimgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $juntjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jultjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agstjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $septjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $okttjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novtjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $destjp_p = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desmgr_p = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        
        //SECTION///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $jantjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febtjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $martjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprtjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meitjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meimgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $juntjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jultjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agstjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $septjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $okttjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novtjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $destjp_s = \App\monpen::where('ULP', $ulp[5])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desmgr_s = \App\monpen::where('ULP', $ulp[6])->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $janblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meiblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desblt_p = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $janblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','01')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $febblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','02')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $marblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','03')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $aprblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','04')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $meiblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','05')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $junblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','06')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $julblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','07')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $agsblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','08')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $sepblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','09')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $oktblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','10')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $novblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','11')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $desblt_s = \App\monpen::where('UP3', 'UP3 BELITUNG')->whereYear('tglpadam',$tahun)->whereMonth('tglpadam','12')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();


        return view('monpen/dashboard',compact('tahun','ulp','janpkp_p','janslt_p','janmtk_p','jantbl_p','jankba_p','febpkp_p','febslt_p','febmtk_p','febtbl_p','febkba_p','marpkp_p','marslt_p','marmtk_p',
        'martbl_p','markba_p','aprpkp_p','aprslt_p','aprmtk_p','aprtbl_p','aprkba_p','meipkp_p','meislt_p','meimtk_p','meitbl_p','meikba_p','junpkp_p','junslt_p','junmtk_p','juntbl_p','junkba_p','julpkp_p',
        'julslt_p','julmtk_p','jultbl_p','julkba_p','agspkp_p','agsslt_p','agsmtk_p','agstbl_p','agskba_p','seppkp_p','sepslt_p','sepmtk_p','septbl_p','sepkba_p','oktpkp_p','oktslt_p','oktmtk_p','okttbl_p',
        'oktkba_p','novpkp_p','novslt_p','novmtk_p','novtbl_p','novkba_p','despkp_p','desslt_p','desmtk_p','destbl_p','deskba_p','janpkp_s','janslt_s','janmtk_s','jantbl_s','jankba_s','febpkp_s','febslt_s',
        'febmtk_s','febtbl_s','febkba_s','marpkp_s','marslt_s','marmtk_s','martbl_s','markba_s','aprpkp_s','aprslt_s','aprmtk_s','aprtbl_s','aprkba_s','meipkp_s','meislt_s','meimtk_s','meitbl_s','meikba_s',
        'junpkp_s','junslt_s','junmtk_s','juntbl_s','junkba_s','julpkp_s','julslt_s','julmtk_s','jultbl_s','julkba_s','agspkp_s','agsslt_s','agsmtk_s','agstbl_s','agskba_s','seppkp_s','sepslt_s','sepmtk_s',
        'septbl_s','sepkba_s','oktpkp_s','oktslt_s','oktmtk_s','okttbl_s','oktkba_s','novpkp_s','novslt_s','novmtk_s','novtbl_s','novkba_s','despkp_s','desslt_s','desmtk_s','destbl_s','deskba_s','janbgk_p',
        'febbgk_p','marbgk_p','aprbgk_p','meibgk_p','junbgk_p','julbgk_p','agsbgk_p','sepbgk_p','oktbgk_p','novbgk_p','desbgk_p','janbgk_s','febbgk_s','marbgk_s','aprbgk_s','meibgk_s','junbgk_s','julbgk_s',
        'agsbgk_s','sepbgk_s','oktbgk_s','novbgk_s','desbgk_s','jantjp_p','janmgr_p','febtjp_p','febmgr_p','martjp_p','marmgr_p','aprtjp_p','aprmgr_p','meitjp_p','meimgr_p','juntjp_p','junmgr_p','jultjp_p',
        'julmgr_p','agstjp_p','agsmgr_p','septjp_p','sepmgr_p','okttjp_p','oktmgr_p','novtjp_p','novmgr_p','destjp_p','desmgr_p','jantjp_s','janmgr_s','febtjp_s','febmgr_s','martjp_s','marmgr_s','aprtjp_s',
        'aprmgr_s','meitjp_s','meimgr_s','juntjp_s','junmgr_s','jultjp_s','julmgr_s','agstjp_s','agsmgr_s','septjp_s','sepmgr_s','okttjp_s','oktmgr_s','novtjp_s','novmgr_s','destjp_s','desmgr_s','janblt_p',
        'febblt_p','marblt_p','aprblt_p','meiblt_p','junblt_p','julblt_p','agsblt_p','sepblt_p','oktblt_p','novblt_p','desblt_p','janblt_s','febblt_s','marblt_s','aprblt_s','meiblt_s','junblt_s','julblt_s',
        'janbbl_p','febbbl_p','marbbl_p','aprbbl_p','meibbl_p','junbbl_p','julbbl_p','agsbbl_p','sepbbl_p','oktbbl_p','novbbl_p','desbbl_p','janbbl_s','febbbl_s','marbbl_s','aprbbl_s','meibbl_s','junbbl_s',
        'julbbl_s','agsbbl_s','sepbbl_s','oktbbl_s','novbbl_s','desbbl_s','agsblt_s','sepblt_s','oktblt_s','novblt_s','desblt_s','pkp_p','slt_p','mtk_p','tbl_p','kba_p','tjp_p','mgr_p','pkp_s','slt_s','mtk_s',
        'tbl_s','kba_s','tjp_s','mgr_s','bgk_p','blt_p','bgk_s','blt_s','bbl_p','bbl_s','from','to',      
    ));
    }
    public function data(Request $request)
    {
        $data_monpen = \App\Monpen::all();
        return view('/monpen/data',compact ('data_monpen'));
    }
    public function input(Request $request)
    {
        $up3_list = \App\up3::all();
        
        return view ('monpen/input',['up3_list' => $up3_list]);
    }
    public function create(Request $request)
    {
        $monpen = \App\Monpen::create($request->all());

        $this->validate($request,[
            'eviden'=>'required'
        ]);
        
        //stores data in public/eviden/original
        foreach ($request->eviden as $photo){
            $fileNameWithExt = $photo->getClientOriginalName();
            //get file name only
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            //get extension of file
            $extension = $photo->getClientOriginalExtension();
            //file name to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            //upload image
            $path = $photo->storeAs('public/eviden/original', $fileNameToStore);

            
            \App\eviden::create([
                'id' => $monpen->id,
                'name' => $fileNameToStore,
            ]);
        }

        return redirect('/monpen/data')->with('sukses','Data Berhasil disimpan');
    }

    public function eviden($id)
    {
        $monpen = \App\monpen::find($id);
        $eviden = \App\Eviden::where('id', $monpen->id)->get();
        $eviden_data = \App\eviden::find($id);
        //$foto = \App\eviden::select('name')->where('id', $monpen->id)->get();
        return view('monpen/eviden', compact('eviden','monpen','eviden_data'));
    }

    public function normal($id)
    {
        $monpen = \App\Monpen::find($id);
        $up3_list = \App\up3::all();
        $ulp_list = \App\ulp::all();
        $penyulang_list = \App\penyulang::all();
        $section_list = \App\section::all();
        return view('monpen/normal',compact ('monpen','up3_list','ulp_list','penyulang_list','section_list'));
    }
    public function hapuseviden($id)
    {
        $eviden = \App\eviden::where('eviden_id',$id);
        $eviden1 = \App\eviden::select('name')->where('eviden_id',$id)->first();
        Storage::delete('public/eviden/original/'.$eviden1->name);
        $eviden->delete($eviden);
        return back()->with('sukses','Eviden Berhasil dihapus');
    }
    public function tambaheviden(Request $request)
    {
        foreach ($request->eviden as $photo){
            $fileNameWithExt = $photo->getClientOriginalName();
            //get file name only
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            //get extension of file
            $extension = $photo->getClientOriginalExtension();
            //file name to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            //upload image
            $path = $photo->storeAs('public/eviden/original', $fileNameToStore);

            
            \App\eviden::create([
                'id' => $request->id,
                'name' => $fileNameToStore,
            ]);
        }
        return back()->with('sukses','Eviden Berhasil ditambah');
    }
    public function update(Request $request, $id)
    {
        
        $monpen = \App\Monpen::find($id);
        $monpen->update($request->all());
        return redirect('/monpen/data')->with('sukses','Data Berhasil diperbaharui');
    }
    public function delete($id)
    {
        $monpen= \App\Monpen::find($id);
        $eviden = \App\eviden::where('id',$id)->get();
        //dd($category_item->cover_image);
        foreach ($eviden as $item){

            Storage::delete('public/eviden/original/'.$item->name);
            $item->delete();
        }
        $monpen->delete();
        return back()->with('sukses','Data Berhasil dihapus');
    }

        
    public function grafikpkp()
    {
        $data_monpen = \App\Monpen::all();
        return view ('monpen/monpenold/grafikpkp');
    }
    public function grafikslt(Request $request)
    {
        //$awal = '2020-08-01';
        //$from = $request->from;
        //$to = $request->to;
        //$from = $request->from;

        $data_monpen = \App\Monpen::all();
        return view ('monpen/monpenold/grafikslt');
    }
    public function grafikmtk()
    {
        $data_monpen = \App\Monpen::all();
        return view ('monpen/monpenold/grafikmtk');
    }
    public function grafiktbl()
    {
        $data_monpen = \App\Monpen::all();
        return view ('monpen/grafiktbl');
    }
    public function grafikkb()
    {
        $data_monpen = \App\Monpen::all();
        return view ('monpen/grafikkb');
    }
    public function grafiktjp()
    {
        $data_monpen = \App\Monpen::all();
        return view ('monpen/grafiktjp');
    }
    public function grafikmgr()
    {
        $data_monpen = \App\Monpen::all();
        return view ('monpen/grafikmgr');
    }
    public function kondisipkp(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        
        $s = \App\penyulang::where('ulp_id','ULP PANGKALPINANG')->pluck('penyulangname');
        
        $p0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p22 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[22])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p23 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[23])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p24 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[24])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p25 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[25])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p26 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[26])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p27 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[27])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p28 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[28])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n22 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[22])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n23 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[23])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n24 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[24])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n25 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[25])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n26 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[26])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n27 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[27])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n28 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[28])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j22 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[22])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j23 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[23])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j24 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[24])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j25 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[25])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j26 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[26])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j27 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[27])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j28 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[28])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP PANGKALPINANG')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP PANGKALPINANG')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP PANGKALPINANG')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP PANGKALPINANG')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP PANGKALPINANG')->count();
        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP PANGKALPINANG')->where('kodefgtm', $k[8])->get()->count();

        return view ('monpen/kondisipkp',compact ('s','p0','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15','p16','p17','p18','p19','p20','p21','p22','p23','p24','p25','p26','p27','p28',
        'n0','n1','n2','n3','n4','n5','n6','n7','n8','n9','n10','n11','n12','n13','n14','n15','n16','n17','n18','n19','n20','n21','n22','n23','n24','n25','n26','n27','n28',
        'j0','j1','j2','j3','j4','j5','j6','j7','j8','j9','j10','j11','j12','j13','j14','j15','j15','j16','j17','j18','j19','j20','j21','j22','j23','j24','j25','j26','j27','j28',
         'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang'));
    }
    public function kondisislt(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        
        $s = \App\penyulang::where('ulp_id','ULP SUNGAILIAT')->pluck('penyulangname');
        
        $p0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP SUNGAILIAT')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP SUNGAILIAT')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP SUNGAILIAT')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP SUNGAILIAT')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP SUNGAILIAT')->count();


        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $k[8])->get()->count();

        return view ('monpen/kondisislt',compact ('s','p0','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15','n0','n1','n2','n3','n4','n5','n6','n7','n8','n9','n10','n11','n12','n13','n14','n15','j0','j1','j2','j3','j4','j5','j6','j7','j8','j9','j10','j11','j12','j13','j14','j15'
                                                    ,'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang'));
    }
    public function kondisimtk(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        
        $s = \App\penyulang::where('ulp_id','ULP MENTOK')->pluck('penyulangname');
        
        $p0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP MENTOK')->count();

        
        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MENTOK')->where('kodefgtm', $k[8])->get()->count();

        return view ('monpen/kondisimtk',compact ('s','p0','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15','p16','p17','p18','p19','p20','p21',
        'n0','n1','n2','n3','n4','n5','n6','n7','n8','n9','n10','n11','n12','n13','n14','n15','n16','n17','n18','n19','n20','n21',
        'j0','j1','j2','j3','j4','j5','j6','j7','j8','j9','j10','j11','j12','j13','j14','j15','j15','j16','j17','j18','j19','j20','j21',
         'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang'));
    }
    public function kondisitbl(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        
        $s = \App\penyulang::where('ulp_id','ULP TOBOALI')->pluck('penyulangname');
        
        $p0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MENTOK')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP TOBOALI')->count();

        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TOBOALI')->where('kodefgtm', $k[8])->get()->count();

        return view ('monpen/kondisitbl',compact ('s','p0','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15','p16','p17','p18','p19',
        'n0','n1','n2','n3','n4','n5','n6','n7','n8','n9','n10','n11','n12','n13','n14','n15','n16','n17','n18','n19',
        'j0','j1','j2','j3','j4','j5','j6','j7','j8','j9','j10','j11','j12','j13','j14','j15','j15','j16','j17','j18','j19',
         'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang'));
    }
    public function kondisikba(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        
        $s = \App\penyulang::where('ulp_id','ULP KOBA')->orderBy('No','ASC')->pluck('penyulangname');
        
        $p0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP KOBA')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP KOBA')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP KOBA')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP KOBA')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP KOBA')->count();

        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP KOBA')->where('kodefgtm', $k[8])->get()->count();

        return view ('monpen/kondisikba',compact ('s','p0','p1','p2','p3','p4','p5',
        'n0','n1','n2','n3','n4','n5',
        'j0','j1','j2','j3','j4','j5',
         'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang'));
    }
    public function kondisitjp(Request $request)
    {
        $from = $request->from;
        $to = $request->to;

        $tahun = $request->tahun;
        
        $s = \App\penyulang::where('ulp_id','ULP TANJUNG PANDAN')->pluck('penyulangname');
        
        $p0 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p6 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p7 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p8 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p9 =  \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p22 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[22])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p23 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[23])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p24 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[24])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p25 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[25])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p26 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[26])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p27 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[27])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p28 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[28])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p29 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[29])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p30 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[30])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p31 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[31])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p32 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[32])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p33 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[33])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p34 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[34])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n22 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[22])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n23 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[23])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n24 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[24])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n25 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[25])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n26 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[26])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n27 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[27])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n28 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[28])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n29 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[29])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n30 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[30])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n31 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[31])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n32 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[32])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n33 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[33])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n34 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[34])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j21 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[21])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j22 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[22])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j23 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[23])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j24 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[24])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j25 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[25])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j26 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[26])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j27 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[27])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j28 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[28])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j29 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[29])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j30 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[30])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j31 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[31])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j32 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[32])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j33 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[33])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j34 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[34])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP TANJUNG PANDAN')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP TANJUNG PANDAN')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP TANJUNG PANDAN')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP TANJUNG PANDAN')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP TANJUNG PANDAN')->count();

        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP TANJUNG PANDAN')->where('kodefgtm', $k[8])->get()->count();



        return view ('monpen/kondisitjp',compact ('s','p0','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15','p16','p17','p18','p19','p20','p21','p22','p23','p24','p25','p26','p27','p28','p29','p30','p31','p32','p33','p34',
        'n0','n1','n2','n3','n4','n5','n6','n7','n8','n9','n10','n11','n12','n13','n14','n15','n16','n17','n18','n19','n20','n21','n22','n23','n24','n25','n26','n27','n28','n29','n30','n31','n32','n33','n34',
        'j0','j1','j2','j3','j4','j5','j6','j7','j8','j9','j10','j11','j12','j13','j14','j15','j15','j16','j17','j18','j19','j20','j21','j22','j23','j24','j25','j26','j27','j28','j29','j30','j31','j32','j33','j34',
         'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','tahun','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang',
        
        
        ));
    }
    public function kondisimgr(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        
        $s = \App\penyulang::where('ulp_id','ULP MANGGAR')->pluck('penyulangname');
        
        $p0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();
        $p20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->where('jenis_PMT','CB')->get()->count();

        $n0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();
        $n20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->whereIn('jenis_PMT',['ACR','GH','LBS'])->get()->count();

        $j0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[0])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[1])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[2])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[3])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[4])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[5])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[6])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[7])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[8])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j9 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[9])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j10 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[10])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j11 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[11])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j12 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[12])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j13 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[13])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j14 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[14])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j15 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[15])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j16 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[16])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j17 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[17])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j18 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[18])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j19 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[19])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $j20 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('penyulang',$s[20])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotal = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MANGGAR')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jtotalpmt = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MANGGAR')->Where('jenis_PMT','CB')->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MANGGAR')->whereIn('jenis_PMT',['ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();
        $jtotalpmtsection = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP', 'ULP MANGGAR')->whereIn('jenis_PMT',['CB','ACR','GH','LBS'])->whereIn('indikasi',['OCR','GFR','TIDAK TERBACA'])->get()->count();

        $jpenyulang = \App\penyulang::Where('ulp_id','ULP MANGGAR')->count();


        $k = \App\kodefgtm::pluck('Kode');

        $k0 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[0])->get()->count();
        $k1 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[1])->get()->count();
        $k2 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[2])->get()->count();
        $k3 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[3])->get()->count();
        $k4 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[4])->get()->count();
        $k5 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[5])->get()->count();
        $k6 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[6])->get()->count();
        $k7 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[7])->get()->count();
        $k8 = \App\monpen::whereBetween('tglpadam',[$from,$to])->where('ULP','ULP MANGGAR')->where('kodefgtm', $k[8])->get()->count();

        return view ('monpen/kondisimgr',compact ('s','p0','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15','p16','p17','p18','p19','p20',
        'n0','n1','n2','n3','n4','n5','n6','n7','n8','n9','n10','n11','n12','n13','n14','n15','n16','n17','n18','n19','n20',
        'j0','j1','j2','j3','j4','j5','j6','j7','j8','j9','j10','j11','j12','j13','j14','j15','j15','j16','j17','j18','j19','j20',
         'k','k0','k1','k2','k3','k4','k5','k6','k7','k8','from','to','jtotal','jtotalpmt','jtotalsection','jtotalpmtsection','jpenyulang'));
    }
    public function getdatamonpen_babel(Request $request)
    {
        if($request->ajax()){           
            //Jika request from_date ada value(datanya) maka
            if(!empty($request->from_date))
            {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if($request->from_date === $request->to_date){
                    //kita filter tanggalnya sesuai dengan request from_date
                    $monpen = \App\monpen::whereDate('tglpadam','=',$request->from_date)->get();
                }
                else{
                    //kita filter dari tanggal awal ke akhir
                    $monpen = \App\monpen::whereBetween('tglpadam', array($request->from_date, $request->to_date))->get();
                }                
            }
            //load data default
            else
            {
                $monpen = \App\monpen::select(['id', 'penyulang', 'UP3', 'ULP', 'tglpadam', 'jampadam', 'tglnyala', 'jamnyala', 'durasi', 'beban', 'ENS', 'jenis_PMT', 'section', 'indikasi', 'R', 'S', 'T', 'N', 'penyebab', 'kodefgtm', 'vendor', 'jsa', 'pengawas', 'cuaca', 'operator', 'created_at', 'updated_at']);
            }
            return datatables()->of($monpen)
            ->addColumn('ACTION1',function($m){
                return '<!-- Split button -->
                <div class="btn-group">
                <a href="/monpen/'.$m->id.'/normal" class="btn btn-xs btn-primary">Normal</a>
                  <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                        <a class="view" 
                        monpen-id='.$m->id.' 
                        penyulang="'.$m->penyulang.'"
                        up3="'.$m->UP3.'"
                        ulp="'.$m->ULP.'"
                        jenispmt="'.$m->jenis_PMT.'"
                        section="'.$m->section.'"
                        indikasi="'.$m->indikasi.'"
                        R="'.$m->R.'"
                        S="'.$m->S.'"
                        T="'.$m->T.'"
                        N="'.$m->N.'"
                        tglpadam="'.$m->tglpadam.'"
                        jampadam="'.$m->jampadam.'"
                        tglnyala="'.$m->tglnyala.'"
                        jamnyala="'.$m->jamnyala.'"
                        durasi="'.$m->durasi.'"
                        beban="'.$m->beban.'"
                        ens="'.$m->ENS.'"
                        penyebab="'.$m->penyebab.'"
                        kodefgtm="'.$m->kodefgtm.'"
                        vendor="'.$m->vendor.'"
                        jsa="'.$m->jsa.'"
                        pengawas="'.$m->pengawas.'"
                        cuaca="'.$m->cuaca.'"
                        operator="'.$m->operator.'"
                        >View</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a onclick="myFunction('.$m->id.')">Eviden</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a class="delete" monpen-id='.$m->id.'>Hapus</a></li>
                  </ul>
                </div>';
            })
            ->rawColumns(['ACTION1'])

            ->toJson();
        }

    }
    public function getdatamonpen_bangka(Request $request)
    {
        if($request->ajax()){           
            //Jika request from_date ada value(datanya) maka
            if(!empty($request->from_date))
            {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if($request->from_date === $request->to_date){
                    //kita filter tanggalnya sesuai dengan request from_date
                    $monpen = \App\monpen::Where('UP3','UP3 BANGKA')->whereDate('tglpadam','=',$request->from_date)->get();
                }
                else{
                    //kita filter dari tanggal awal ke akhir
                    $monpen = \App\monpen::Where('UP3','UP3 BANGKA')->whereBetween('tglpadam', array($request->from_date, $request->to_date))->get();
                }                
            }
            //load data default
            else
            {
                $monpen = \App\monpen::select(['id', 'penyulang', 'UP3', 'ULP', 'tglpadam', 'jampadam', 'tglnyala', 'jamnyala', 'durasi', 'beban', 'ENS', 'jenis_PMT', 'section', 'indikasi', 'R', 'S', 'T', 'N', 'penyebab', 'kodefgtm', 'vendor', 'jsa', 'pengawas', 'cuaca', 'operator', 'created_at', 'updated_at'])->Where('UP3','UP3 BANGKA');
            }
            return datatables()->of($monpen)
            ->addColumn('ACTION',function($m){
                return '<a href="/monpen/'.$m->id.'/normal" class="btn btn-warning">Normal</a>';
            })
            ->addColumn('ACTION1',function($m){
                return '<button type="button" class="btn btn-danger delete" monpen-id='.$m->id.'>Hapus</button>';

            })
            ->addColumn('ACTION2',function($m){
                return '<a href="/monpen/'.$m->id.'/view" class="btn btn-success">View</a>';

            })
            ->rawColumns(['ACTION','ACTION1','ACTION1'])

            ->toJson();
        }

    }
    public function getdatamonpen_belitung(Request $request)
    {
        if($request->ajax()){           
            //Jika request from_date ada value(datanya) maka
            if(!empty($request->from_date))
            {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if($request->from_date === $request->to_date){
                    //kita filter tanggalnya sesuai dengan request from_date
                    $monpen = \App\monpen::Where('UP3','UP3 BELITUNG')->whereDate('tglpadam','=',$request->from_date)->get();
                }
                else{
                    //kita filter dari tanggal awal ke akhir
                    $monpen = \App\monpen::Where('UP3','UP3 BELITUNG')->whereBetween('tglpadam', array($request->from_date, $request->to_date))->get();
                }                
            }
            //load data default
            else
            {
                $monpen = \App\monpen::select(['id', 'penyulang', 'UP3', 'ULP', 'tglpadam', 'jampadam', 'tglnyala', 'jamnyala', 'durasi', 'beban', 'ENS', 'jenis_PMT', 'section', 'indikasi', 'R', 'S', 'T', 'N', 'penyebab', 'kodefgtm', 'vendor', 'jsa', 'pengawas', 'cuaca', 'operator', 'created_at', 'updated_at'])->Where('UP3','UP3 BELITUNG');
            }
            return datatables()->of($monpen)
            ->addColumn('ACTION',function($m){
                return '<a href="/monpen/'.$m->id.'/normal" class="btn btn-warning">Normal</a>';
            })
            ->addColumn('ACTION1',function($m){
                return '<button type="button" class="btn btn-danger delete" monpen-id='.$m->id.'>Hapus</button>';

            })
            ->addColumn('ACTION2',function($m){
                return '<a href="/monpen/'.$m->id.'/view" class="btn btn-success">View</a>';

            })

            ->rawColumns(['ACTION','ACTION1','ACTION2'])

            ->toJson();
        }

    }
    public function getdatamonpen_ULP(Request $request)
    {
        if($request->ajax()){           
            //Jika request from_date ada value(datanya) maka
            if(!empty($request->from_date))
            {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if($request->from_date === $request->to_date){
                    //kita filter tanggalnya sesuai dengan request from_date
                    $monpen = \App\monpen::whereDate('tglpadam','=',$request->from_date)->get();
                }
                else{
                    //kita filter dari tanggal awal ke akhir
                    $monpen = \App\monpen::whereBetween('tglpadam', array($request->from_date, $request->to_date))->get();
                }                
            }
            //load data default
            else
            {
                $monpen = \App\monpen::select(['id', 'penyulang', 'UP3', 'ULP', 'tglpadam', 'jampadam', 'tglnyala', 'jamnyala', 'durasi', 'beban', 'ENS', 'jenis_PMT', 'section', 'indikasi', 'R', 'S', 'T', 'N', 'penyebab', 'kodefgtm', 'vendor', 'jsa', 'pengawas', 'cuaca', 'operator', 'created_at', 'updated_at']);
            }
            return datatables()->of($monpen)
           
            ->toJson();
        }

    }
    public function findUlpName(Request $request)
    {
        $ulp_list = \App\ulp::select('ulpname','idulpname')->orderBy('No')->where('up3_id', $request
            ->idname)->take(100)->get();
            return response()->json($ulp_list);
    }
    public function findPenyulangName(Request $request)
    {
        $penyulang_list = \App\penyulang::select('penyulangname','idpenyulangname')->where('ulp_id', $request
            ->idulpname)->take(100)->get();
            return response()->json($penyulang_list);
    }
    public function findSectionName(Request $request)
    {
        $section_list = \App\section::select('sectionname','idsectionname')->where('penyulang_id', $request
            ->idpenyulangname)->take(100)->get();
            return response()->json($section_list);
    }
    public function findJenispmt(Request $request)
    {
        $jenispmt_list = \App\section::select('jenispmt')->where('idsectionname', $request
            ->idsectionname)->first();
            return response()->json($jenispmt_list);
    }
    public function exportexcel() 
        {
            return Excel::download(new MonpenExport, 'Monpen_UIW_BABEL.xlsx');
        }
    public function exportexcel_BGK() 
    {
        return Excel::download(new MonpenExport_BGK, 'Monpen_UP3_BANGKA.xlsx');
    }

    public function exportexcel_BLT() 
    {
        return Excel::download(new MonpenExport_BLT, 'Monpen_UP3_BELITUNG.xlsx');
    }


        ///ULP SUNGAILIAT//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public function getAllPenyulangSLT()
        {
            $SLT_array = array();
            $SLT_p = \App\penyulang::Where('ulp_id','ULP SUNGAILIAT')->orderBy('No','ASC')->pluck('penyulangname');
            $SLT_p = json_decode($SLT_p);
            return  $SLT_p;
            if (! empty($SLT_p)){
                foreach ($SLT_p as $SLT_pen) {
                $SLT_array [$SLT_pen] = $SLT_pen;
                }
            }
            return $SLT_array;
        }
        public function getPenyulangSumber($penyulang)
        {
                {
                    //'2020-08-01','2020-08-30'
                    //$from = '2020-08-01';
                    //$to = '2020-08-30';
                $penyulang_count = \App\Monpen::where('penyulang' , $penyulang)->where('jenis_PMT','CB')->get()->count();
                    //return $penyulang_count;
                    return $penyulang_count;
    
                    
                }
           
        }
        
        public function getPenyulangSebagian($penyulang)
        {
            
            $penyulang_count = \App\Monpen::where('penyulang', $penyulang)->whereIn('jenis_PMT', ['ACR','GH'])->get()->count();
            return $penyulang_count;
        }
    
        public function getPenyulangSLT()
        {
           
            $penyulang_count_array = array();
            $penyulang_count_array_sumber = array();
            $penyulang_count_array_sebagian = array();
            $SLT_array = $this->getAllPenyulangSLT();
            $penyulang_name_array = array();
            if (! empty($SLT_array)) {
                foreach($SLT_array as $SLT_pen){
                    $penyulang_count_sumber = $this->getPenyulangSumber ($SLT_pen);
                    $penyulang_count_sebagian = $this->getPenyulangSebagian ($SLT_pen);
                    array_push($penyulang_count_array_sumber, $penyulang_count_sumber);
                    array_push($penyulang_count_array_sebagian, $penyulang_count_sebagian);
                    array_push($penyulang_name_array, $SLT_pen);
                }
            }
            $penyulang_count_array = array (
                        'penyulang' => $penyulang_name_array,
                        'penyulang_SLT_sumber' => $penyulang_count_array_sumber,
                        'penyulang_SLT_sebagian' => $penyulang_count_array_sebagian,
                            );
            return $penyulang_count_array;
        }
    
        public function getAllKode()
        {
                $kode_array = array();
                $kode_f = \App\kodefgtm::Select('Kode')->orderBy('No','ASC')->pluck('Kode');
                $kode_f = json_decode($kode_f);
                return $kode_f;
                if (! empty($kode_f)){
                    foreach ($kode_f as $kode_fg) {
                    $kode_array [$kode_fg] = $kode_fg;
                    }
                }
                return $kode_array;
        }
        public function getKodeCount($kodefgtm)
        {
            //$kodefgtm = 'I-1 KOMPONEN JTM';
                $kode_count = \App\Monpen::where('ULP','ULP SUNGAILIAT')->where('kodefgtm', $kodefgtm)->get()->count();
                return $kode_count;
        }
        public function getKodeSLT()
            {
            $kode_count_array = array();
            $kodeSLT_array = $this->getAllKode();
            $kode_name_array = array();
            if (! empty($kodeSLT_array)) {
                foreach($kodeSLT_array as $kode_fg){
                    $kode_count_SLT = $this->getKodeCount ($kode_fg);
                    
    
                    array_push($kode_count_array, $kode_count_SLT);
                    array_push($kode_name_array, $kode_fg);
                }
            }
            $kode_count_array = array (
               
                        'kode' => $kode_name_array,
                        'kode_SLT' => $kode_count_array,
                        
                            );
            return $kode_count_array;
            }
    

}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


use App\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{
    public function changepassword()
    {
        return view('monpen.changepassword');
    }
    public function updatepassword (Request $request)
    {

        if(!Hash::check($request->get('oldpassword'), Auth::user()->password)) {
            return back()->with('error','Password tidak valid');

        }
        if(strcmp($request->get('oldpassword'), $request->get('password')) == 0) {
            return back()->with('error','Password baru tidak boleh sama dengan password lama');
        }
        if(!strcmp($request->get('password'), $request->get('Cpassword')) == 0) {
            return back()->with('error','Password baru tidak sama dengan konfirmasi password baru');
        }

        $user = Auth::User();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return back()->with('message','Password sudah berhasil diganti');
    }
}

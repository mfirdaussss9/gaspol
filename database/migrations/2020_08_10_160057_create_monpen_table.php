<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonpenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monpen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('penyulang');
            $table->string('UP3');
            $table->string('ULP');
            $table->string('tglpadam');
            $table->string('jampadam');
            $table->string('tglnyala');
            $table->string('jamnyala');
            $table->string('durasi');
            $table->string('beban');
            $table->string('ENS');
            $table->string('jenis_PMT');
            $table->string('section');
            $table->string('indikasi');
            $table->string('R');
            $table->string('S');
            $table->string('T');
            $table->string('N');
            $table->string('penyebab');
            $table->string('kodefgtm');
            $table->string('vendor');
            $table->string('jsa');
            $table->string('pengawas');
            $table->string('cuaca');
            $table->string('operator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monpen');
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Monpen::class, function (Faker $faker) {
    return [
        'penyulang' => $faker->name,
        'UP3' => $faker->randomElement(['BELITUNG','BANGKA']),
        'ULP' => $faker->randomElement(['PANGKALPINANG','SUNGAILIAT','MUNTOK']),
        'tglpadam' => '',
        'jampadam' => '',
        'tglnyala' => '',
        'jamnyala' => '',
        'durasi' => '',
        'beban' => '',
        'ENS' => '',
        'jenis_PMT' => '',
        'section' => '',
        'indikasi' => '',
        'R' => '',
        'S' => '',
        'T' => '',
        'N' => '',
        'penyebab' => $faker->address,
        'kodefgtm' => '',
        'vendor' => '',
        'jsa' => '',
        'pengawas' => '',
        'cuaca' => '',
        'operator' => '',
    ];
});

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/login','AuthController@login')->name('login');
Route::post('/postlogin','AuthController@postlogin');
Route::get('/logout','AuthController@logout');
Route::get('/monpen/changepassword','ChangePasswordController@changepassword')->name('changepassword');
Route::post('/monpen/updatepassword','ChangePasswordController@updatepassword')->name('updatepassword');


Route::group(['middleware'=>['auth','CheckRole:UIW.BABEL,UP3.BANGKA,UP3.BELITUNG,ULP']],function(){

    
    Route::get('/monpen/dashboard','MonpenController@dashboard');
    Route::get('/monpen/data','MonpenController@data');
    Route::get('/monpen/input','MonpenController@input');

    Route::get('/monpen/kondisipkp','MonpenController@kondisipkp');
    Route::get('/monpen/kondisislt','MonpenController@kondisislt');
    Route::get('/monpen/kondisimtk','MonpenController@kondisimtk');
    Route::get('/monpen/kondisitbl','MonpenController@kondisitbl');
    Route::get('/monpen/kondisikba','MonpenController@kondisikba');
    Route::get('/monpen/kondisitjp','MonpenController@kondisitjp');
    Route::get('/monpen/kondisimgr','MonpenController@kondisimgr');

    Route::get('/monpen/monpenold/grafikmtk','MonpenController@grafikmtk');


    Route::post('/monpen/create','MonpenController@create');
    Route::get('/monpen/{id}/normal','MonpenController@normal');
    Route::post('/monpen/{id}/update','MonpenController@update');
    Route::get('monpen/{id}/delete','MonpenController@delete');

    Route::get('/monpen/{id}/eviden', 'MonpenController@eviden');
    Route::get('monpen/{id}/hapuseviden', 'MonpenController@hapuseviden');
    Route::post('/monpen/tambaheviden', 'MonpenController@tambaheviden')->name('tambah.eviden');



    Route::get('/findUlpName','MonpenController@findUlpname');
    Route::get('/findPenyulangName','MonpenController@findPenyulangname');
    Route::get('/findSectionName','MonpenController@findSectionname');
    Route::get('/findJenispmt','MonpenController@findJenispmt');

    Route::get('monpen/exportexcel','MonpenController@exportexcel');
    Route::get('monpen/exportexcel_BGK','MonpenController@exportexcel_BGK');
    Route::get('monpen/exportexcel_BLT','MonpenController@exportexcel_BLT');

    Route::get('monpen/exportpdf','MonpenController@exportpdf');

    Route::get('/monpen/grafik','GrafikController@grafik');
    Route::get('/getPenyulangPKP','GrafikController@getPenyulangPKP');
    
    Route::get('/monpen/slt','GrafikController@slt');
    Route::get('/PenyulangSLT','GrafikController@PenyulangSLT');
    Route::get('/PenyulangSecSLT','GrafikController@PenyulangSecSLT');

    Route::get('/monpen/slt2021','GrafikController@slt2021');
    Route::get('/PenyulangSLT2021','GrafikController@PenyulangSLT2021');
    Route::get('/PenyulangSecSLT2021','GrafikController@PenyulangSecSLT2021');

    Route::get('/getKodePKP','GrafikController@getKodePKP');
    
    Route::get('/monpen/monpenold/grafikslt','MonpenController@grafikslt');
    Route::get('/getPenyulangSLT','MonpenController@getPenyulangSLT');
    Route::get('/getKodeSLT','MonpenController@getKodeSLT');
    Route::get('/tahunSumber','GrafikController@tahunSumber');

    Route::get('/getPenyulangSumberjan', 'GrafikController@getPenyulangSumberjan');
    Route::get('/getPenyulangSumberTotal', 'GrafikController@getPenyulangSumberTotal');
        Route::get('/getAllPenyulangPKP','GrafikController@getAllPenyulangPKP');
        });

        Route::get('getdatamonpen_babel',[
            'uses' => 'MonpenController@getdatamonpen_babel',
            'as' => 'ajax.get.data.monpen.babel',
        ]);

        Route::get('getdatamonpen_bangka',[
            'uses' => 'MonpenController@getdatamonpen_bangka',
            'as' => 'ajax.get.data.monpen.bangka',
        ]);

        Route::get('getdatamonpen_belitung',[
            'uses' => 'MonpenController@getdatamonpen_belitung',
            'as' => 'ajax.get.data.monpen.belitung',
        ]);

        Route::get('getdatamonpen_ULP',[
            'uses' => 'MonpenController@getdatamonpen_ULP',
            'as' => 'ajax.get.data.monpen.ULP',
        ]);




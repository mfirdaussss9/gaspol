<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/login','AuthController@login')->name('login');
Route::post('/postlogin','AuthController@postlogin');
Route::get('/logout','AuthController@logout');

Route::group(['middleware'=>'auth'],function(){

    Route::get('/monpen/dashboard','MonpenController@dashboard');
    Route::get('/monpen/data','MonpenController@data');
    Route::get('/monpen/bgk','MonpenController@bgk');




    Route::get('/monpen/input','MonpenController@input');

    Route::get('/monpen/input','MonpenController@input');
    Route::get('/findUlpName','MonpenController@findUlpname');
    Route::get('/findPenyulangName','MonpenController@findPenyulangname');
    Route::get('/findSectionName','MonpenController@findSectionname');
    Route::get('/findJenispmt','MonpenController@findJenispmt');

    Route::get('/monpen/grafikpkp','MonpenController@grafikpkp');
    Route::get('/getPenyulangPKP','MonpenController@getPenyulangPKP');
    Route::get('/getKodePKP','MonpenController@getKodePKP');

    Route::get('/monpen/grafikslt','MonpenController@grafikslt');
    Route::get('/getPenyulangSLT','MonpenController@getPenyulangSLT');
    Route::get('/getKodeSLT','MonpenController@getKodeSLT');

    Route::get('/monpen/grafikmtk','MonpenController@grafikmtk');
    Route::get('/getPenyulangMTK','MonpenController@getPenyulangMTK');
    Route::get('/getKodeMTK','MonpenController@getKodeMTK');

    Route::get('/monpen/grafiktbl','MonpenController@grafiktbl');
    Route::get('/getPenyulangTBL','MonpenController@getPenyulangTBL');
    Route::get('/getKodeTBL','MonpenController@getKodeTBL');

    Route::get('/monpen/grafikkb','MonpenController@grafikkb');
    Route::get('/getPenyulangKB','MonpenController@getPenyulangKB');
    Route::get('/getKodeKB','MonpenController@getKodeKB');

    Route::get('/monpen/grafiktjp','MonpenController@grafiktjp');
    Route::get('/getPenyulangTJP','MonpenController@getPenyulangTJP');
    Route::get('/getKodeTJP','MonpenController@getKodeTJP');

    Route::get('/monpen/grafikmgr','MonpenController@grafikmgr');
    Route::get('/getPenyulangMGR','MonpenController@getPenyulangMGR');
    Route::get('/getKodeMGR','MonpenController@getKodeMGR');

    Route::get('/monpen/kondisipkp','MonpenController@kondisipkp');
    Route::get('/monpen/kondisislt','MonpenController@kondisislt');
    Route::get('/monpen/kondisimtk','MonpenController@kondisimtk');
    Route::get('/monpen/kondisitbl','MonpenController@kondisitbl');
    Route::get('/monpen/kondisikba','MonpenController@kondisikba');
    Route::get('/monpen/kondisitjp','MonpenController@kondisitjp');
    Route::get('/monpen/kondisimgr','MonpenController@kondisimgr');


    Route::get('monpen/export','MonpenController@export');

    Route::post('/monpen/create','MonpenController@create');
    Route::get('/monpen/{id}/normal','MonpenController@normal');
    Route::post('/monpen/{id}/update','MonpenController@update');
    Route::get('monpen/{id}/delete','MonpenController@delete');
    
});

Route::get('getdatamonpen',[
    'uses' => 'MonpenController@getdatamonpen',
    'as' => 'ajax.get.data.monpen',
]);



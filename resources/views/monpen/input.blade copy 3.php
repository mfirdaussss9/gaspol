@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>


<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Masukan Data Penyulang</h3>
								</div>
								<div class="panel-body">
                                <div class="row">
                                <form action="/monpen/create" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group row">
                                    <div class="col-sm-4 bg-warning" >
                                        <label for="exampleInputEmail1">UP3</label>
                                        <select name=UP3 class="form-control form-control-sm UP3" id="up3_id" required>
                                        <option value="0" disabled="true" selected="true">Pilih UP3</option>
                                        @foreach ($up3_list as $cat)
                                            <option value="{{$cat->idname}}">{{$cat->name}}</option>
                                        @endforeach
                                        </select>                                   
                                    </div>
                                    <div class="col-sm-4 bg-warning">
                                        <label for="exampleInputEmail1">ULP</label>
                                        <select name="ULP" class="form-control form-control-sm ulpname" id="ulp_id" required >
                                        <option value="0" disabled="true" selected="true">Pilih ULP</option>
									</select>                                   
                                    </div>
                                    <div class="col-sm-4 bg-warning" >
                                        <label for="exampleInputEmail1">PENYULANG</label>
                                        <select name=penyulang class="form-control form-control-sm penyulangname" id="penyulang_id" required >
                                        <option value="0" disabled="true" selected="true">Pilih Penyulang</option>
									</select>                                   
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-4 bg-warning">
                                        <label for="exampleInputEmail1">SECTION</label>
                                        <select name=section class="form-control form-control-sm sectionname" id="section_id" required >
                                        <option value="0" disabled="true" selected="true">Pilih Section</option>
									</select>                                   
                                    </div>
                                    <div class="col-sm-2 bg-warning">
                                        <label for="exampleInputEmail1">JENIS PMT</label>
                                        <input name="jenis_PMT" type="text" class="form-control form-control-sm jenispmt" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="exampleInputEmail1">INDIKASI</label>
                                        <select name=indikasi class="form-control form-control-sm" >
                                        <option value="0" disabled="true" selected="true">Pilih Indikasi</option>
                                        <option value="OCR">OCR</option>
                                        <option value="GFR">GFR</option>
                                        <option value="UFR">UFR</option>
                                        <option value="HAR">HAR</option>
                                        <option value="PMD">PMD</option>
                                        <option value="TIDAK TERBACA">TIDAK TERBACA</option>
									</select>                                   
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">R</label>
                                        <input name="R" type="text" class="form-control form-control-sm" id="R" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">S</label>
                                        <input name="S" type="text" class="form-control form-control-sm" id="S" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">T</label>
                                        <input name="T" type="text" class="form-control form-control-sm" id="T" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">N</label>
                                        <input name="N" type="text" class="form-control form-control-sm" id="N" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-3 bg-warning">
                                        <label for="exampleInputEmail1">TANGGAL PADAM</label>
                                        <input name="tglpadam" type="date" class="form-control form-control-sm" id="tglpadam" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="col-sm-3 bg-warning">
                                        <label for="exampleInputEmail1">JAM PADAM</label>
                                        <input name="jampadam" type="time" class="form-control form-control-sm" id="jampadam" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">TANGGAL NYALA</label>
                                        <input name="tglnyala" type="date" class="form-control form-control-sm" id="tglnyala" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">JAM NYALA</label>
                                        <input name="jamnyala" type="time" class="form-control form-control-sm" id="jamnyala" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">DURASI</label>
                                        <input name="durasi" type="text" class="form-control form-control-sm" id="durasi" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">BEBAN</label>
                                        <input name="beban" type="text" class="form-control form-control-sm" id="beban" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">ENS</label>
                                        <input name="ENS" type="text" class="form-control form-control-sm" id="ENS" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">VENDOR</label>
                                        <input name="vendor" type="text" class="form-control form-control-sm" id="vendor" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">NOMOR JSA</label>
                                        <input name="jsa" type="text" class="form-control form-control-sm" id="jsa" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">PENGAWAS</label>
                                        <input name="pengawas" type="text" class="form-control form-control-sm" id="pengawas" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">PENYEBAB</label>
                                        <input name="penyebab" type="text" class="form-control form-control-sm" id="penyebab" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">KODE FGTM</label>
                                        <select name=indikasi class="form-control form-control-sm" >
                                        <option value="0" disabled="true" selected="true">Pilih Kode FGTM</option>
                                        <option value="I-1 KOMPONEN JTM">I-1 KOMPONEN JTM</option>
                                        <option value="I-2 PERALATAN JTM">I-2 PERALATAN JTM</option>
                                        <option value="I-3 TRAFO DLL">I-3 TRAFO DLL</option>
                                        <option value="I-4 TIANG">I-4 TIANG</option>
                                        <option value="E-1 POHON">E-1 POHON</option>
                                        <option value="E-2 BENCANA ALAM">E-2 BENCANA ALAM</option>
                                        <option value="E-3 PEK. PIHAK III/BINATANG">E-3 PEK. PIHAK III/BINATANG</option>
                                        <option value="E-4 LAYANG2/UMBUL2">E-4 LAYANG2/UMBUL2</option>
                                    </select>                                   
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">CUACA</label>
                                        <input name="cuaca" type="text" class="form-control form-control-sm" id="cuaca" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-3 bg-warning">
                                        <label for="exampleInputEmail1">OPERATOR</label>
                                        <input name="operator" type="text" class="form-control form-control-sm" id="operator" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="col-md-3">
                                    <br />
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>

                                </div>
								</div>
							</div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    $(document).on('change','.UP3',function(){
            //console.log("hmm ini berubah");

            var cat_id=$(this).val();
            //console.log(cat_id);
            var div = $(this).parent().parent();

            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findUlpName')!!}',
                data:{'idname':cat_id},
                success:function(data) {
                    //console.log('success');
                    
                    //console.log(data);

                    //console.log(data.length);
                    op+='<option value="0" selected disabled>Pilih ULP</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].idulpname+'">'+data[i].ulpname+'</option>';
                    }
                    div.find('.ulpname').html(" ");
                    div.find('.ulpname').append(op);
                },
                error:function(){

                }

            });
    });

    $(document).on('change','.ulpname',function(){
        var cat_id=$(this).val();
            //console.log(cat_id);
            var div = $(this).parent().parent();

            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findPenyulangName')!!}',
                data:{'idulpname':cat_id},
                success:function(data) {
                    //console.log('success');
                    
                    //console.log(data);

                    //console.log(data.length);
                    op+='<option value="0" selected disabled>Pilih Penyulang</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].idpenyulangname+'">'+data[i].penyulangname+'</option>';
                    }
                    div.find('.penyulangname').html(" ");
                    div.find('.penyulangname').append(op);
                },
                error:function(){

                }

            });
    });

    $(document).on('change','.penyulangname',function(){
        var cat_id=$(this).val();
            //console.log(cat_id);
            var div = $(this).parent().parent().parent();

            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findSectionName')!!}',
                data:{'idpenyulangname':cat_id},
                success:function(data) {
                    //console.log('success');
                    
                    //console.log(data);

                    //console.log(data.length);

                    op+='<option value="0" selected disabled>Pilih Section</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].idsectionname+'">'+data[i].sectionname+'</option>';
                    }
                    div.find('.sectionname').html(" ");
                    div.find('.sectionname').append(op);
                    
                },
                error:function(){

                }

            });
    });

    $(document).on('change','.sectionname',function(){
            var jenispmt_id=$(this).val();
            var a = $(this).parent().parent()
            console.log(jenispmt_id);
            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findJenispmt')!!}',
                data:{'idsectionname':jenispmt_id},
                dataType:'json', //return data willbe json
                success:function(data) {
                    console.log("jenispmt");
                    
                    console.log(data.jenispmt);

                    a.find('.jenispmt').val(data.jenispmt);
                },
                error:function(){

                }

            });
    });


});
</script>        
        
@stop

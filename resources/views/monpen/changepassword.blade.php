@extends('layouts.master')

@section('content')


<div class="main">
    <div class="main-content">
    @if(Session('error'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session('error')}}
            </div>
        @endif
    
        @if(Session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session('message')}}
            </div>
        @endif


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
						<div class="panel-heading">
                            <h3 class="panel-title"><strong>GANTI PASSWORD | USER : {{Auth::user()->name}} </strong></h3>
                        </div>  
                            <div class="panel-body">
                                <form method="POST" action="{{ route ('updatepassword') }}">
                                    {{csrf_field()}}

                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Password Lama</label>
                                    <div class="col-sm-3">
                                    <input type="password" class="form-control" name="oldpassword" id="oldpassword">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Password Baru</label>
                                    <div class="col-sm-3">
                                    <input type="password" class="form-control" name="password" id="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Konfirmasi Password Baru</label>
                                    <div class="col-sm-3">
                                    <input type="password" class="form-control" name="Cpassword" id="Cpassword">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                                
                                </form>
                                
                    </div>
                </div>
				</div>
            </div>
        </div>
    </div>
</div>    
@stop



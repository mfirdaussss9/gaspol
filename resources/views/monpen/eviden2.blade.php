@extends('layouts.master')

@section('content')


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>

<div class="main">
    <div class="main-content">
        @if(Session('sukses'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session('sukses')}}
            </div>
        @endif
    


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><strong>LIHAT EVIDEN | ID : {{$monpen->id}} | PENYULANG : {{$monpen->penyulang}}</strong></h3>
								</div>
								<div class="panel-body">
                                <div class="row">
                                    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#upload">
                                        Tambah Eviden
                                    </button>
                                <br> <br />
                                <table id="myTable" class="table table-bordered table-striped text-center" >
                                        <thead>
                                            <tr>
                                                <th style="width:1%;">ID</th>
                                                <th>Nama File</th>
                                                <th>Eviden</th>
                                                <th style="width: 10%;">Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($eviden as $key => $item)
                                                <tr>
                                                        <td>{{$item->eviden_id}}</td>
                                                        <td>{{$item->name}}</td>
                                                        <td><img style="width:70%;" src="{{asset('storage/eviden/original/'.$item->name)}}" alt="image"> </td>

                                                    
                                                        <td>
                                                            <button type="button" class="btn btn-danger delete" eviden-id="{{$item->eviden_id}}">Hapus</button>
                                                        </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah eviden-->
<div class="modal fade" id="upload" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      
        <h4 class="modal-title" id="exampleModalLabel">TAMBAH EVIDEN
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></h4>
        </button>
       
      </div>
      <div class="modal-body">
        <form action="{{route('tambah.eviden')}}" method="POST" name="form1" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group row">
            <div class="col-sm-3">
                <input type="hidden" name="id" value="{{$monpen->id}}">
                <label for="exampleInputEmail1">UPLOAD EVIDEN </label> 
                <input type="file" name="eviden[]" multiple="true">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit"  class="btn btn-primary">Simpan</button>
        </div>
        </form>
        </div>
    </div>
    </div>

<script>
$('body').on('click','.delete',function(){
            var eviden_id = $(this).attr('eviden-id');
            swal({
                title:"Perhatian!!!",
                text:"Anda yakin ingin menghapus eviden id "+eviden_id+" ini ?",
                icon: "warning",
                buttons:true,
                dangerMode: true,
                showCancelButton: true,
            })
            .then((willDelete) => {
                console.log(willDelete) ;
                if (willDelete){
                    window.location = "/monpen/"+eviden_id+"/hapuseviden";
                }
            });
        });
</script>
<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>

@stop
@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title"><strong>REKAPAN GANGGUAN DI PMT TAHUN 2020</strong></h3>
                </div>

                <!-- TABEL SUMBER 2020-->
                    <table class="table table-bordered table-hover table-striped tg" id="RM_table">
                    <colgroup>
                        <col style="width: 29px">
                        <col style="width: 101px">
                        <col style="width: 132px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 41px">
                        <col style="width: 57px">
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="tg-9wq8">No</th>
                            <th class="tg-9wq8">SUMBER</th>
                            <th class="tg-baqh">PENYULANG</th>
                            <th class="tg-9wq8">JAN</th>
                            <th class="tg-9wq8">FEB</th>
                            <th class="tg-9wq8">MAR</th>
                            <th class="tg-9wq8">APR</th>
                            <th class="tg-9wq8">MEI</th>
                            <th class="tg-9wq8">JUN</th>
                            <th class="tg-9wq8">JUL</th>
                            <th class="tg-9wq8">AGS</th>
                            <th class="tg-9wq8">SEP</th>
                            <th class="tg-9wq8">OKT</th>
                            <th class="tg-9wq8">NOV</th>
                            <th class="tg-9wq8">DES</th>
                            <th class="tg-9wq8">TOTAL</th>
                        </tr>
                        </thead>
                    <tbody id="RM_body"></tbody>
                    </table>

                                    </div>
                                    </div>


                                    <div class="col-md-12">
                <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title"><strong>REKAPAN GANGGUAN DI ACR & GH TAHUN 2020</strong></h3>
                </div>
<!---TABEL SECTION --->
                <table class="table table-bordered table-hover table-striped tg" id="RM1_table">
                <colgroup>
                    <col style="width: 29px">
                    <col style="width: 101px">
                    <col style="width: 132px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 41px">
                    <col style="width: 57px">
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="tg-9wq8">No</th>
                        <th class="tg-9wq8">PENYULANG</th>
                        <th class="tg-baqh">SECTION</th>
                        <th class="tg-9wq8">JAN</th>
                        <th class="tg-9wq8">FEB</th>
                        <th class="tg-9wq8">MAR</th>
                        <th class="tg-9wq8">APR</th>
                        <th class="tg-9wq8">MEI</th>
                        <th class="tg-9wq8">JUN</th>
                        <th class="tg-9wq8">JUL</th>
                        <th class="tg-9wq8">AGS</th>
                        <th class="tg-9wq8">SEP</th>
                        <th class="tg-9wq8">OKT</th>
                        <th class="tg-9wq8">NOV</th>
                        <th class="tg-9wq8">DES</th>
                        <th class="tg-9wq8">TOTAL</th>
                    </tr>
                    </thead>
            <tbody id="RM1_body"></tbody>
            <tfoot id="RM1_tfoot"></tfoot>
                    

            </table>
            </div>
            </div>



                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- TABEL SUMBER -->

 <script>
  $(document).ready(function () {

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              type: "GET",
              url: '/PenyulangSLT',
              dataType :'json',
                
    

    success: function(data) {
                 //console.log(data);
          /* $(document).ready(function () {
                $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) > 6) {
                        $(this).css("background-color", "#d6dad5");
                    }
                    
                    });
            });

            $(document).ready(function () {
                $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) <= 6) {
                        $(this).css("background-color", "#ff4343");
                    }
                  
                   });
            });
            $(document).ready(function () {
                    $("#RM_table td:nth-child(4)").each(function () {
                        if (parseInt($(this).text(), 10) <= 2) {
                            $(this).css("background-color", "yellow");
                        }
                    
                    });
                });
                $(document).ready(function () {
                    $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) == 0) {
                            $(this).css("background-color", "#8fff82");
                        }
                    });
                });*/

                 $.each(data, function(i, RM){
                      $('#RM_body').append($("<tr>")
                      .append($("<td>",).append(RM.No))
                      .append($("<td>",).append(RM.pmtsum_nama))
                      .append($("<td>").append(RM.penyulang_nama_PMT))
                      .append($("<td>").append(RM.penyulang_PMT.rekam01))
                      .append($("<td>").append(RM.penyulang_PMT.rekam02))
                      .append($("<td>").append(RM.penyulang_PMT.rekam03))
                      .append($("<td>").append(RM.penyulang_PMT.rekam04))
                      .append($("<td>").append(RM.penyulang_PMT.rekam05))
                      .append($("<td>").append(RM.penyulang_PMT.rekam06))
                      .append($("<td>").append(RM.penyulang_PMT.rekam07))
                      .append($("<td>").append(RM.penyulang_PMT.rekam08))
                      .append($("<td>").append(RM.penyulang_PMT.rekam09))
                      .append($("<td>").append(RM.penyulang_PMT.rekam10))
                      .append($("<td>").append(RM.penyulang_PMT.rekam11))
                      .append($("<td>").append(RM.penyulang_PMT.rekam12))
                      .append($("<td>").append(RM.penyulang_PMT.rekam13))
                      );


                 }); 
              },
              error: function (error) {
                  console.log(error);
              }

        });
    });
 </script>



<!-- TABEL SECTION -->
<script>
  $(document).ready(function () {

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              type: "GET",
              url: '/PenyulangSecSLT',
              dataType :'json',
                
    

    success: function(data) {
                 //console.log(data);
          /* $(document).ready(function () {
                $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) > 6) {
                        $(this).css("background-color", "#d6dad5");
                    }
                    
                    });
            });

            $(document).ready(function () {
                $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) <= 6) {
                        $(this).css("background-color", "#ff4343");
                    }
                  
                   });
            });
            $(document).ready(function () {
                    $("#RM_table td:nth-child(4)").each(function () {
                        if (parseInt($(this).text(), 10) <= 2) {
                            $(this).css("background-color", "yellow");
                        }
                    
                    });
                });
                $(document).ready(function () {
                    $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) == 0) {
                            $(this).css("background-color", "#8fff82");
                        }
                    });
                });*/

                 $.each(data, function(i, RM1){
                      $('#RM1_body').append($("<tr>")
                      .append($("<td>",).append(RM1.No))
                      .append($("<td>",).append(RM1.pensec_nama))
                      .append($("<td>").append(RM1.penyulang_nama_SEC))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam01))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam02))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam03))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam04))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam05))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam06))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam07))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam08))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam09))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam10))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam11))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam12))
                      .append($("<td>").append(RM1.penyulang_SEC.rekam13))
                      );
                 }); 
              },
              error: function (error) {
                  console.log(error);
              }

        });
    });
 </script>


<script>
     /*$(document).ready(function () {

    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            

        $.ajax({
              type: "GET",
              url: '/tahunSumber',
              data: {
                  tahun:tahun
              },
              success: function(data) {
                  console.log(data);


                  
                  
                  
              },
              error: function (error) {
                  console.log(error);
              }
        })
    });
});           */       
    </script> 

 <style>
 
 #RM_table_wrapper {
  width: 100%;
}
#RM_table {
  font-family: Verdana, sans-serif;
  color: black;
  border-collapse: collapse;
  border: 1px solid black;
  margin: 10px auto;
  text-align: center;
  width: 60%;
  max-width: 800px;
}
#RM_table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
#RM_table th {
	font-weight: 600;
  padding: 0.5em;
}
#RM_table td, #RM_table th, #RM_table caption {
  padding: 0.5em;
  border: 1px solid black;

}
#RM_table thead, tfoot tr{
  background: #FFDEAD;
  color: black;
}

#RM1_table_wrapper {
  width: 100%;
}
#RM1_table {
  font-family: Verdana, sans-serif;
  color: black;
  border-collapse: collapse;
  border: 1px solid black;
  margin: 10px auto;
  text-align: center;
  width: 60%;
  max-width: 800px;
}
#RM1_table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
#RM1_table th {
	font-weight: 600;
  padding: 0.5em;
}
#RM1_table td, #RM1_table th, #RM1_table caption {
  padding: 0.5em;
  border: 1px solid black;

}
#RM1_table thead, tfoot tr{
  background: #FFDEAD;
  color: black;
}

 </style>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>



       
       
@stop
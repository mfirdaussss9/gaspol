@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                  <h3 class="panel-title"><strong>ULP SUNGAILIAT</strong></h3>
                    <div class="right">
                  <h4>JUMLAH GANGGUAN : {{$jtotal}}</h4>
                    </div>
                </div>
                      <div class="panel-body">
                        <div class="pencarian">
                              <form action="" method="GET" class="form-inline">
                              <div class="form-group mb-2">
                                  <input type="date" name="from" value="{{$from}}" class="form-control">
                              </div>
                              <medium id="sd" class="text-muted">
                                s.d.
                              </medium>
                              <div class="form-group">
                                  <input type="date" name="to" value="{{$to}}" class="form-control">
                                </div>
                              <button class="btn btn-md btn-primary" type="submit">CARI</button>
                              <a href="/monpen/kondisislt" class="btn btn-warning btn-md">ULANG</a>
                              </form>
                          </div>
                          <div id="grafikbar"></div>
                          <center> <strong>REKAM MEDIS KONDISI PENYULANG : </strong>      
                          <p> 
                            <button type="button" class="btn btn-success" readonly>Sehat (0)</button>
                            <button type="button" class="btn btn-warning" readonly>Meriang (1-2)</button>
                            <button type="button" class="btn btn-danger" readonly>Sakit (3-6)</button>
                            <button type="button" class="btn btn-dark" readonly>Kronis (=>7)</button> 
                          </p>
                          </center>
                          
                          <table id="datatable" class="table table-responsive">
                            <thead>
                              <tr>
                                <th>PENYULANG</th>
                                <th>PMT</th>
                                <th>SECTION</th>
                                <th>JUMLAH</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                              <tr>
                                <th>{{$s[0]}}</th>
                                <td>{{$p0}}</td>
                                <td>{{$n0}}</td>
                                <td>{{$j0}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[1]}}</th>
                                <td>{{$p1}}</td>
                                <td>{{$n1}}</td>
                                <td>{{$j1}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[2]}}</th>
                                <td>{{$p2}}</td>
                                <td>{{$n2}}</td>
                                <td>{{$j2}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[3]}}</th>
                                <td>{{$p3}}</td>
                                <td>{{$n3}}</td>
                                <td>{{$j3}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[4]}}</th>
                                <td>{{$p4}}</td>
                                <td>{{$n4}}</td>
                                <td>{{$j4}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[5]}}</th>
                                <td>{{$p5}}</td>
                                <td>{{$n5}}</td>
                                <td>{{$j5}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[6]}}</th>
                                <td>{{$p6}}</td>
                                <td>{{$n6}}</td>
                                <td>{{$j6}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[7]}}</th>
                                <td>{{$p7}}</td>
                                <td>{{$n7}}</td>
                                <td>{{$j7}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[8]}}</th>
                                <td>{{$p8}}</td>
                                <td>{{$n8}}</td>
                                <td>{{$j8}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[9]}}</th>
                                <td>{{$p9}}</td>
                                <td>{{$n9}}</td>
                                <td>{{$j9}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[10]}}</th>
                                <td>{{$p10}}</td>
                                <td>{{$n10}}</td>
                                <td>{{$j10}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[11]}}</th>
                                <td>{{$p11}}</td>
                                <td>{{$n11}}</td>
                                <td>{{$j11}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[12]}}</th>
                                <td>{{$p12}}</td>
                                <td>{{$n12}}</td>
                                <td>{{$j12}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[13]}}</th>
                                <td>{{$p13}}</td>
                                <td>{{$n13}}</td>
                                <td>{{$j13}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[14]}}</th>
                                <td>{{$p14}}</td>
                                <td>{{$n14}}</td>
                                <td>{{$j14}}</td>
                              </tr>
                              <tr>
                                <th>{{$s[15]}}</th>
                                <td>{{$p15}}</td>
                                <td>{{$n15}}</td>
                                <td>{{$j15}}</td>
                              </tr>
                              <tfoot>
                              <tr>
                                <th>TOTAL</th>
                                <td>{{$jtotalpmt}}</td>
                                <td>{{$jtotalsection}}</td>
                                <td>{{$jtotalpmtsection}}</td>
                              </tr>
                              <tfoot>
                            </tbody>
                          </table>
                          <figure class="highcharts-figure">
                          <div id="grafikpie"></div>
                          </figure>
                      </div>
							    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>

#grafikbar {
  height: 800px;
}

.highcharts-figure, .highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  margin: 1em auto;
}
#datatable_wrapper {
  width: 100%;
}
#datatable {
  font-family: Verdana, sans-serif;
  color: black;
  border-collapse: collapse;
  border: 1px solid black;
  margin: 10px auto;
  text-align: center;
  width: 60%;
  max-width: 800px;
}
#datatable caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
#datatable th {
	font-weight: 600;
  padding: 0.5em;
}
#datatable td, #datatable th, #datatable caption {
  padding: 0.5em;
  border: 1px solid black;

}
#datatable thead, tfoot tr{
  background: #FFDEAD;
  color: black;
}



table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
bottom: .5em;
}

.highcharts-figure, .highcharts-data-table table {
  min-width: 320px; 
  max-width: 660px;
  margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

</style>

<script>
        $(document).ready(function () {
            $('#datatable').DataTable({
            "order": [[ 3, "desc" ]],
            "paging": false,
            });
            $('.dataTables_length').addClass('bs-select');
        });
</script>

  <script>
  $(document).ready(function () {
                $("#datatable td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) > 6) {
                        $(this).css("background-color", "#d6dad5");
                    }
                  
                   });
            });
  $(document).ready(function () {
                $("#datatable td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) <= 6) {
                        $(this).css("background-color", "#ff4343");
                    }
                  
                   });
            });
        $(document).ready(function () {
                $("#datatable td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) <= 2) {
                        $(this).css("background-color", "yellow");
                    }
                  
                   });
            });
            $(document).ready(function () {
                $("#datatable td:nth-child(4)").each(function () {
                  if (parseInt($(this).text(), 10) == 0) {
                        $(this).css("background-color", "#8fff82");
                    }
                  });
            });

//column 3
$(document).ready(function () {
                $("#datatable td:nth-child(3)").each(function () {
                    if (parseInt($(this).text(), 10) > 6) {
                        $(this).css("background-color", "#d6dad5");
                    }
                  
                   });
            });
  $(document).ready(function () {
                $("#datatable td:nth-child(3)").each(function () {
                    if (parseInt($(this).text(), 10) <= 6) {
                        $(this).css("background-color", "#ff4343");
                    }
                  
                   });
            });
        $(document).ready(function () {
                $("#datatable td:nth-child(3)").each(function () {
                    if (parseInt($(this).text(), 10) <= 2) {
                        $(this).css("background-color", "yellow");
                    }
                  
                   });
            });
            $(document).ready(function () {
                $("#datatable td:nth-child(3)").each(function () {
                  if (parseInt($(this).text(), 10) == 0) {
                        $(this).css("background-color", "#8fff82");
                    }
                  });
            });

//column 2
$(document).ready(function () {
                $("#datatable td:nth-child(2)").each(function () {
                    if (parseInt($(this).text(), 10) > 6) {
                        $(this).css("background-color", "#d6dad5");
                    }
                  
                   });
            });
  $(document).ready(function () {
                $("#datatable td:nth-child(2)").each(function () {
                    if (parseInt($(this).text(), 10) <= 6) {
                        $(this).css("background-color", "#ff4343");
                    }
                  
                   });
            });
        $(document).ready(function () {
                $("#datatable td:nth-child(2)").each(function () {
                    if (parseInt($(this).text(), 10) <= 2) {
                        $(this).css("background-color", "yellow");
                    }
                  
                   });
            });
            $(document).ready(function () {
                $("#datatable td:nth-child(2)").each(function () {
                  if (parseInt($(this).text(), 10) == 0) {
                        $(this).css("background-color", "#8fff82");
                    }
                  });
            });
  </script>

<script>
  $(function() {//from   w w w. java 2  s .  c  om
  var chart;
  var sortData = function(chartSource) {
     console.time('sorting');
    var series = chartSource.series;
    var axis = chartSource.xAxis[0];
    var categories = [];
    if ($.isArray(series)) {
      var ser = $.grep(series, function(ser, seriesIndex) {
        return ser.visible;
      })[0];
      $.each(ser.data, function(dataIndex, datum) {
        var obj = {
          name: datum.category,
          index: dataIndex,
          stackTotal: datum.stackTotal
        }
        categories.push(obj);
      });
    }
    categories.sort(function(a, b) {
      var aName = a.name.toLowerCase();
      var bName = b.name.toLowerCase();
      var aTotal = a.stackTotal;
      var bTotal = b.stackTotal;
      if (aTotal === bTotal) {
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
      } else {
        return ((aTotal > bTotal) ? -1 : ((aTotal < bTotal) ? 1 : 0));
      }
    });
    var mappedIndex = $.map(categories, function(category, index) {
      return category.index;
    });
    categories = $.map(categories, function(category, index) {
      return category.name;
    });
   // console.log(categories);
    //console.log(mappedIndex);
    axis.setCategories(categories);
    $.each(series, function(seriesIndex, ser) {
      var data = $.map(mappedIndex, function(mappedIndex, origIndex) {
        return ser.data[mappedIndex].y;
      });
      ser.setData(data, false);
    });
    chartSource.redraw();
      console.timeEnd('sorting');
  };
  $(document).ready(function() {
    chart = new Highcharts.Chart({
      data: {
        table: 'datatable',
        endRow: {{$jpenyulang}}

    },
      chart: {
        renderTo: 'grafikbar',
        type: 'bar'
      },
      title: {
        text: 'GRAFIK GANGGUAN PENYULANG ULP SUNGAILIAT'
      },
     
      yAxis: {
        allowDecimals: false,
        title: {
          text: 'kali'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: ( // theme
              Highcharts.defaultOptions.title.style &&
              Highcharts.defaultOptions.title.style.color
            ) || 'gray',
          }
          
        },
        reversedStacks: false,

      },
      plotOptions: {
        series: {
          stacking: 'normal',
          dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
          formatter: function(){
          return (this.y!=0)?this.y:"";
          }
                },
          events: {
            hide: function() {
              sortData(chart);
            },
            show: function() {
              sortData(chart);
            }
          }
        }
      },
      series: [{
        name: '',
        data: [],
        stack: '',
        zones: [{
            value: 1,
            color: 'green',
            
        }, {
            value: 3,
            color: 'yellow',
        }, {
            value: 7,
            color: 'red',
        }, {
            color: 'black',
        }]
    }, {
        name: '',
        data: [],
        stack: '',
        zones: [{
            value: 1,
            color: 'green',
        }, {
            value: 3,
            color: 'yellow',
        }, {
            value: 7,
            color: 'red',
        }, {
            color: 'black',
        }]
    }, {
        name: '',
        data: [],
        stack: '',
        visible : false,
        
    }],
    tooltip: {
        formatter: function () {
            return '<b>' + this.point.name.toUpperCase() + '</b><br/>' +
                'Trip' + ' ' + this.point.y + ' ' + 'kali' + ' ' + 'di'+ ' ' + this.series.name;
        }
    },
    exporting: {
            csv: {
                columnHeaderFormatter: function(item, key) {
                    if (!item || item instanceof Highcharts.Axis) {
                        return 'PENYULANG'
                    } else {
                        return item.name;
                    }
                }
            },
        },
    }, function(chart) {
      sortData(chart);
    });
  });
});
</script>

<script>
// Radialize the colors
Highcharts.setOptions({
  colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
      radialGradient: {
        cx: 0.5,
        cy: 0.3,
        r: 0.7
      },
      stops: [
        [0, color],
        [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
      ]
    };
  })
});

// Build the chart
Highcharts.chart('grafikpie', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'PRESENTASE PENYEBAB GANGGUAN ULP SUNGAILIAT'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>:({point.y}) {point.percentage:.1f} %',
        connectorColor: 'silver'
      }
    }
  },
  series: [{
    name: '%',
    data: [
      { name: 'BELUM DITEMUKAN', y: {{$k0}} },
      { name: 'I-1 KOMPONEN JTM', y: {{$k1}} },
      { name: 'I-2 PERALATAN JTM', y: {{$k2}} },
      { name: 'I-3 TRAFO DLL', y: {{$k3}} },
      { name: 'I-4 TIANG', y: {{$k4}} },
      { name: 'E-1 POHON', y: {{$k5}} },
      { name: 'E-2 BENCANA ALAM', y: {{$k6}} },
      { name: 'E-3 PEK. PIHAK III/BINATANG', y: {{$k7}} },
      { name: 'E-4 LAYANG2/UMBUL2', y: {{$k8}} },
    ]
  }],
  exporting: {
        csv: {
            columnHeaderFormatter: function(item, key) {
                if (!item || item instanceof Highcharts.Axis) {
                    return 'PENYEBAB'
                } else {
                    return item.name;
                }
            }
        }
    },
});
</script>
@stop



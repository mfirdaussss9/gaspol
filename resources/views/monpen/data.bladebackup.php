@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<div class="main">
    <div class="main-content">
        @if(Session('sukses'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session('sukses')}}
            </div>
        @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                                    <h3 class="panel-title"><strong>MONITORING PENYULANG</strong></h3>
                                    <div class="right">
                                    <a href = "/monpen/export" class="btn btn-warning btn-sm">EKSPOR KE MS.EXCEL</a>

                                    <a href="/monpen/input" class="btn btn-primary btn-sm">TAMBAH +</a>
                                    </div>
                                </div>
                                <div class="panel-body" style="overflow: scroll; height: 500px;">
                                
									<table class="table table-hover" id="datatable">
										<thead>
											<tr>
                                                <th>NORMAL</th>
                                                <th>HAPUS</th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>SECTION</th>
                                                <th>JENIS PMT</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</thead>
										<tbody>
                                        </tbody>
                                        <tfoot>
											<tr>
                                                <th></th>
                                                <th></th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>JENIS PMT</th>
                                                <th>SECTION</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</tfoot>
                                    </table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            order: [12, 'desc'],
            autoWidth: false,
            processing:true,
            serverside:true,
            ajax:"{{route('ajax.get.data.monpen')}}",
            columns:[
                {data:'ACTION',name:'ACTION'},
                {data:'ACTION1',name:'ACTION1'},
                {data:'UP3',name:'UP3'},
                {data:'ULP',name:'ULP'},
                {data:'penyulang',name:'penyulang'},
                {data:'section',name:'section'},
                {data:'jenis_PMT',name:'jenis_PMT'},
                {data:'indikasi',name:'indikasi'},
                {data:'R',name:'R'},
                {data:'S',name:'S'},
                {data:'T',name:'T'},
                {data:'N',name:'N'},
                {data:'tglpadam',name:'tglpadam'},
                {data:'jampadam',name:'jampadam'},
                {data:'tglnyala',name:'tglnyala'},
                {data:'jamnyala',name:'jamnyala'},
                {data:'durasi',name:'durasi'},
                {data:'beban',name:'beban'},
                {data:'ENS',name:'ENS'},
                {data:'penyebab',name:'penyebab'},
                {data:'kodefgtm',name:'kodefgtm'},
                {data:'vendor',name:'vendor'},
                {data:'jsa',name:'jsa'},
                {data:'pengawas',name:'pengawas'},
                {data:'cuaca',name:'cuaca'},
                {data:'operator',name:'operator'}
            ],
            initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        }
        });

        $('body').on('click','.delete',function(){
            var monpen_id = $(this).attr('monpen-id');
            swal({
                title:"Perhatian!!!",
                text:"Anda yakin ingin menghapus ID "+monpen_id+" ini ?",
                icon: "warning",
                buttons:true,
                dangerMode: true,
                showCancelButton: true,
            })
            .then((willDelete) => {
                console.log(willDelete) ;
                if (willDelete){
                    window.location = "/monpen/"+monpen_id+"/delete";
                }
            });
        });
    });
</script>

<script>
     $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" size="5px"/>' );
    } );
 
 
} );

</script>
<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>
<style>
div.panel-body {
    overflow-x: auto;
    white-space: nowrap;
}
div.panel-body [class*="col"]{  /* TWBS v2 */
    display: inline-block;
    float: none; /* Very important */
}
</style>
<script type="text/javascript"> 
    $(document).ready(function () {
       
    });
</script>
@stop



@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<div class="main">
    <div class="main-content">
        @if(Session('sukses'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session('sukses')}}
            </div>
        @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                                    <h3 class="panel-title"><strong>MONITORING PENYULANG</strong></h3>
                                    
                                    <div class="right">
                                    @if(auth()->user()->role == 'UP3.BANGKA' )
                                        <a href = "/monpen/exportexcel_BGK" class="btn btn-success btn-sm">EKSPOR KE MS.EXCEL</a>
                                        <a href="/monpen/input" class="btn btn-primary btn-sm"><span>TAMBAH </span><i class="fa fa-plus-circle"></i></a>

                                    @elseif(auth()->user()->role == 'UP3.BELITUNG')
                                        <a href = "/monpen/exportexcel_BLT" class="btn btn-success btn-sm">EKSPOR KE MS.EXCEL</a>
                                        <a href="/monpen/input" class="btn btn-primary btn-sm"><span>TAMBAH </span><i class="fa fa-plus-circle"></i></a>

                                    @elseif(auth()->user()->role == 'UIW.BABEL' )
                                        <a href = "/monpen/exportexcel" class="btn btn-success btn-sm">EKSPOR KE MS.EXCEL</a>
                                        <a href="/monpen/input" class="btn btn-primary btn-sm"><span>TAMBAH </span><i class="fa fa-plus-circle"></i></a>
                                    @elseif(auth()->user()->role == 'ULP' )
                                    <a href = "/monpen/exportexcel" class="btn btn-success btn-sm">EKSPOR KE MS.EXCEL</a>
                                    @endif
                                    </div>
                                </div>
                                
                                <div class="panel-body" style="overflow: scroll; height: 500px;">
                                    <!-- MULAI DATE RANGE PICKER -->
                                    <div class="pencarian">
                                        <div class="row input-daterange" data-provide="datepicker"  data-date-format="yyyy-mm-dd" align="center">
                                        <div class="col-sm-2">
                                            <input type="text" name="from_date" id="from_date" class="form-control" placeholder="Mulai Tanggal"
                                                readonly />
                                        </div>
                                        <medium id="sd" class="text-muted">
                                             s.d.
                                        </medium>
                                        <div class="col-sm-2">
                                            <input type="text" name="to_date" id="to_date" class="form-control" placeholder="Akhir Tanggal"
                                                readonly />
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" name="filter" id="filter" class="btn btn-primary btn-md">CARI</button>
                                            <button type="button" name="refresh" id="refresh" class="btn btn-warning btn-md">ULANG</button>
                                        </div>
                                    </div>
                                    </div>
                                    <br />
                                    <!-- AKHIR DATE RANGE PICKER -->
                                    @if(auth()->user()->role == 'UP3.BANGKA' )
                                        <table class="table table-hover" id="datatable_bangka">
                                        <thead>
											<tr>
                                                <th>NORMAL</th>
                                                <th>HAPUS</th>
                                                <th>VIEW</th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>SECTION</th>
                                                <th>JENIS PMT</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</thead>
										<tbody>
                                        </tbody>
                                        <tfoot>
											<tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>JENIS PMT</th>
                                                <th>SECTION</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</tfoot>
                                    </table>
                                    @elseif(auth()->user()->role == 'UP3.BELITUNG')
                                        <table class="table table-hover" id="datatable_belitung">
                                        <thead>
											<tr>
                                                <th>NORMAL</th>
                                                <th>HAPUS</th>
                                                <th>VIEW</th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>SECTION</th>
                                                <th>JENIS PMT</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</thead>
										<tbody>
                                        </tbody>
                                        <tfoot>
											<tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>JENIS PMT</th>
                                                <th>SECTION</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</tfoot>
                                    </table>
                                    @elseif(auth()->user()->role == 'UIW.BABEL' )
                                        <table class="table table-hover" id="datatable_babel">
                                        <thead>
											<tr>
                                                <th>--TINDAKAN--</th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>SECTION</th>
                                                <th>JENIS PMT</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</thead>
										<tbody>
                                        </tbody>
                                        <tfoot>
											<tr>
                                                <th></th>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>JENIS PMT</th>
                                                <th>SECTION</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</tfoot>
                                    </table>
                                    @elseif(auth()->user()->role == 'ULP' )
                                        <table class="table table-hover" id="datatable_ULP">
                                        <thead>
											<tr>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>SECTION</th>
                                                <th>JENIS PMT</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</thead>
										<tbody>
                                        </tbody>
                                        <tfoot>
											<tr>
                                                <th>UP3</th>
                                                <th>ULP</th>
                                                <th>PENYULANG</th>
                                                <th>JENIS PMT</th>
                                                <th>SECTION</th>
                                                <th>INDIKASI</th>
                                                <th>R</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>N</th>
                                                <th>TANGGAL PADAM</th>
                                                <th>JAM PADAM</th>
                                                <th>TANGGAL NYALA</th>
                                                <th>JAM NYALA</th>
                                                <th>DURASI</th>
                                                <th>BEBAN</th>
                                                <th>ENS</th>
                                                <th>PENYEBAB/PEKERJAAN</th>
                                                <th>KODE FGTM</th>
                                                <th>VENDOR</th>
                                                <th>NO. JSA</th>
                                                <th>PENGAWAS</th>
                                                <th>CUACA</th>
                                                <th>OPERATOR</th>
											</tr>
										</tfoot>
                                    </table>
                                    @endif
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--BABEL-->
<script>
 $(document).ready(function () {
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
 //jalankan function load_data diawal agar data ter-load
  load_data();

 //Iniliasi datepicker pada class input
            $('#filter').click(function () {
                var from_date = $('#from_date').val(); 
                var to_date = $('#to_date').val(); 
                if (from_date != '' && to_date != '') {
                    $('#datatable_babel').DataTable().destroy();
                    load_data(from_date, to_date);
                } else {
                    alert('Both Date is required');
                }
            });
            $('#refresh').click(function () {
                $('#from_date').val('');
                $('#to_date').val('');
                $('#datatable_babel').DataTable().destroy();
                load_data();
            });

    function load_data(from_date = '', to_date = '') {
        $('#datatable_babel').DataTable({
            order: [[11, 'desc'], [12, 'desc']],
            processing:true,
            serverside:true,
            ajax: {
                        url: "{{ route('ajax.get.data.monpen.babel') }}",
                        type: 'GET',
                        data:{from_date:from_date, to_date:to_date} //jangan lupa kirim parameter tanggal 
                    },
            columns:[
                {data:'ACTION1',name:'ACTION1', width: '6%'},
                {data:'UP3',name:'UP3'},
                {data:'ULP',name:'ULP'},
                {data:'penyulang',name:'penyulang'},
                {data:'section',name:'section'},
                {data:'jenis_PMT',name:'jenis_PMT'},
                {data:'indikasi',name:'indikasi'},
                {data:'R',name:'R'},
                {data:'S',name:'S'},
                {data:'T',name:'T'},
                {data:'N',name:'N'},
                {data:'tglpadam',name:'tglpadam'},
                {data:'jampadam',name:'jampadam'},
                {data:'tglnyala',name:'tglnyala'},
                {data:'jamnyala',name:'jamnyala'},
                {data:'durasi',name:'durasi'},
                {data:'beban',name:'beban'},
                {data:'ENS',name:'ENS'},
                {data:'penyebab',name:'penyebab'},
                {data:'kodefgtm',name:'kodefgtm'},
                {data:'vendor',name:'vendor'},
                {data:'jsa',name:'jsa'},
                {data:'pengawas',name:'pengawas'},
                {data:'cuaca',name:'cuaca'},
                {data:'operator',name:'operator'}
            ],
            initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
                } );
                }
                });
                }
            });
        

        $('body').on('click','.delete',function(){
            var monpen_id = $(this).attr('monpen-id');
            swal({
                title:"Perhatian!!!",
                text:"Anda yakin ingin menghapus ID:"+monpen_id+" dan eviden ?",
                icon: "warning",
                buttons:true,
                dangerMode: true,
                showCancelButton: true,
            })
            .then((willDelete) => {
                console.log(willDelete) ;
                if (willDelete){
                    window.location = "/monpen/"+monpen_id+"/delete";
                }
            });
        });

        
        $('body').on('click','.view',function(){
            var monpen_id = $(this).attr('monpen-id');
            var up3 =  $(this).attr('up3');
            var ulp =  $(this).attr("ulp");
            var tglpadam =  $(this).attr('tglpadam');
            var jampadam =  $(this).attr('jampadam');
            var tglnyala =  $(this).attr('tglnyala');
            var jamnyala =  $(this).attr('jamnyala');
            var penyulang =  $(this).attr('penyulang');
            var jenispmt =  $(this).attr('jenispmt');
            var indikasi =  $(this).attr('indikasi');
            var r =  $(this).attr('r');
            var s =  $(this).attr('s');
            var t =  $(this).attr('t');
            var n =  $(this).attr('n');
            var durasi =  $(this).attr('durasi');
            var beban =  $(this).attr('beban');
            var ens =  $(this).attr('ens');
            var section =  $(this).attr('section');
            var kodefgtm =  $(this).attr('kodefgtm');
            var penyebab =  $(this).attr('penyebab');
            var vendor =  $(this).attr('vendor');
            var jsa =  $(this).attr('jsa');
            var pengawas =  $(this).attr('pengawas');
            var cuaca =  $(this).attr('cuaca');
            var operator =  $(this).attr('operator');
            
            if(indikasi == 'OCR' || indikasi == 'GFR' || indikasi == 'TIDAK TERBACA') {
            swal({
                title:"LAPORAN PENYULANG",
                html:   '<div class ="textcopy" id="divClipboard">'+
                        '*LAPORAN GANGGUAN PENYULANG* <br />' +
                        'Id : '+monpen_id+' <br />' + 
                        'Penyulang : '+penyulang+' <br />' + 
                        'UP3 : '+up3+' <br />' + 
                        'ULP : '+ulp+' <br />' + 
                        'Jenis PMT : '+jenispmt+' <br />' + 
                        'Section : '+section+' <br />' + 
                        'Indikasi : '+indikasi+' <br />' + 
                        'R : '+r+' <br />' + 
                        'S : '+s+' <br />' + 
                        'T : '+t+' <br />' + 
                        'N : '+n+' <br />' + 
                        'Waktu Padam : '+tglpadam+' '+jampadam+'<br />'+
                        'Waktu Nyala : '+tglnyala+' '+jamnyala+' <br />'+
                        'Durasi (Menit) : '+durasi+'<br />' + 
                        'Beban (A) : '+beban+' <br />' + 
                        'ENS (kWh) : '+ens+' <br />' + 
                        'Penyebab : '+penyebab+' <br />' + 
                        'Kode FGTM : '+kodefgtm+' <br />' + 
                        'Cuaca : '+cuaca+' <br />' + 
                        'Operator : '+operator+' <br />' +
                        '</div>',
                icon: "warning",
                buttons:true,
                dangerMode: true,
            })
            } else {
                swal({
                title:"LAPORAN PENYULANG",
                html:   '<div class ="textcopy">'+
                        '*LAPORAN PEMADAMAN PENYULANG* <br />' +
                        'Id : '+monpen_id+' <br />' + 
                        'Penyulang : '+penyulang+' <br />' + 
                        'UP3 : '+up3+' <br />' + 
                        'ULP : '+ulp+' <br />' + 
                        'Jenis PMT : '+jenispmt+' <br />' + 
                        'Section : '+section+' <br />' + 
                        'Indikasi : '+indikasi+' <br />' + 
                        'Waktu Padam : '+tglpadam+' '+jampadam+'<br />'+
                        'Waktu Nyala : '+tglnyala+' '+jamnyala+' <br />'+
                        'Durasi (menit) : '+durasi+'<br />' + 
                        'Beban (A) : '+beban+' <br />' + 
                        'ENS (kWh) : '+ens+' <br />' + 
                        'Pekerjaan : '+penyebab+' <br />' + 
                        'Vendor : '+vendor+' <br />' + 
                        'JSA : '+jsa+' <br />' + 
                        'Pengawas : '+pengawas+' <br />' + 
                        'Cuaca : '+cuaca+' <br />' + 
                        'Operator : '+operator+' <br />' +
                        '</div>',
                icon: "warning",
                buttons:true,
                dangerMode: true,
            })
            }
        });


</script>

<style>

div.textcopy {
    text-align: left;
}
</style>

<script>
     $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable_babel tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" size="5px"/>' );
    });
 
});

</script>
<!-- BANGKA -->
<script>
 $(document).ready(function () {
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
 //jalankan function load_data diawal agar data ter-load
  load_data();

 //Iniliasi datepicker pada class input
            $('#filter').click(function () {
                var from_date = $('#from_date').val(); 
                var to_date = $('#to_date').val(); 
                if (from_date != '' && to_date != '') {
                    $('#datatable_bangka').DataTable().destroy();
                    load_data(from_date, to_date);
                } else {
                    alert('Both Date is required');
                }
            });
            $('#refresh').click(function () {
                $('#from_date').val('');
                $('#to_date').val('');
                $('#datatable_bangka').DataTable().destroy();
                load_data();
            });

    function load_data(from_date = '', to_date = '') {
        $('#datatable_bangka').DataTable({
            order: [[14, 'desc'], [15, 'desc']],
            processing:true,
            serverside:true,
            ajax: {
                        url: "{{ route('ajax.get.data.monpen.bangka') }}",
                        type: 'GET',
                        data:{from_date:from_date, to_date:to_date} //jangan lupa kirim parameter tanggal 
                    },
            columns:[
                {data:'ACTION',name:'ACTION'},
                {data:'ACTION1',name:'ACTION1'},
                {data:'ACTION2',name:'ACTION2'},
                {data:'UP3',name:'UP3'},
                {data:'ULP',name:'ULP'},
                {data:'penyulang',name:'penyulang'},
                {data:'section',name:'section'},
                {data:'jenis_PMT',name:'jenis_PMT'},
                {data:'indikasi',name:'indikasi'},
                {data:'R',name:'R'},
                {data:'S',name:'S'},
                {data:'T',name:'T'},
                {data:'N',name:'N'},
                {data:'tglpadam',name:'tglpadam'},
                {data:'jampadam',name:'jampadam'},
                {data:'tglnyala',name:'tglnyala'},
                {data:'jamnyala',name:'jamnyala'},
                {data:'durasi',name:'durasi'},
                {data:'beban',name:'beban'},
                {data:'ENS',name:'ENS'},
                {data:'penyebab',name:'penyebab'},
                {data:'kodefgtm',name:'kodefgtm'},
                {data:'vendor',name:'vendor'},
                {data:'jsa',name:'jsa'},
                {data:'pengawas',name:'pengawas'},
                {data:'cuaca',name:'cuaca'},
                {data:'operator',name:'operator'}
            ],
            initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
                } );
                }
                });
                }
            });
        

        $('body').on('click','.delete',function(){
            var monpen_id = $(this).attr('monpen-id');
            swal({
                title:"Perhatian!!!",
                text:"Anda yakin ingin menghapus ID:"+monpen_id+" dan eviden ?",
                icon: "warning",
                buttons:true,
                dangerMode: true,
                showCancelButton: true,
            })
            .then((willDelete) => {
                console.log(willDelete) ;
                if (willDelete){
                    window.location = "/monpen/"+monpen_id+"/delete";
                }
            });
        });
</script>

<script>
     $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable_bangka tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" size="5px"/>' );
    });
 
});

</script>
<!-- BELITUNG -->
<script>
 $(document).ready(function () {
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
 //jalankan function load_data diawal agar data ter-load
  load_data();

 //Iniliasi datepicker pada class input
            $('#filter').click(function () {
                var from_date = $('#from_date').val(); 
                var to_date = $('#to_date').val(); 
                if (from_date != '' && to_date != '') {
                    $('#datatable_belitung').DataTable().destroy();
                    load_data(from_date, to_date);
                } else {
                    alert('Both Date is required');
                }
            });
            $('#refresh').click(function () {
                $('#from_date').val('');
                $('#to_date').val('');
                $('#datatable_belitung').DataTable().destroy();
                load_data();
            });

    function load_data(from_date = '', to_date = '') {
        $('#datatable_belitung').DataTable({
            order: [[14, 'desc'], [15, 'desc']],
            processing:true,
            serverside:true,
            ajax: {
                        url: "{{ route('ajax.get.data.monpen.belitung') }}",
                        type: 'GET',
                        data:{from_date:from_date, to_date:to_date} //jangan lupa kirim parameter tanggal 
                    },
            columns:[
                {data:'ACTION',name:'ACTION'},
                {data:'ACTION1',name:'ACTION1'},
                {data:'ACTION2',name:'ACTION2'},
                {data:'UP3',name:'UP3'},
                {data:'ULP',name:'ULP'},
                {data:'penyulang',name:'penyulang'},
                {data:'section',name:'section'},
                {data:'jenis_PMT',name:'jenis_PMT'},
                {data:'indikasi',name:'indikasi'},
                {data:'R',name:'R'},
                {data:'S',name:'S'},
                {data:'T',name:'T'},
                {data:'N',name:'N'},
                {data:'tglpadam',name:'tglpadam'},
                {data:'jampadam',name:'jampadam'},
                {data:'tglnyala',name:'tglnyala'},
                {data:'jamnyala',name:'jamnyala'},
                {data:'durasi',name:'durasi'},
                {data:'beban',name:'beban'},
                {data:'ENS',name:'ENS'},
                {data:'penyebab',name:'penyebab'},
                {data:'kodefgtm',name:'kodefgtm'},
                {data:'vendor',name:'vendor'},
                {data:'jsa',name:'jsa'},
                {data:'pengawas',name:'pengawas'},
                {data:'cuaca',name:'cuaca'},
                {data:'operator',name:'operator'}
            ],
            initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
                } );
                }
                });
                }
            });
        

        $('body').on('click','.delete',function(){
            var monpen_id = $(this).attr('monpen-id');
            swal({
                title:"Perhatian!!!",
                text:"Anda yakin ingin menghapus ID:"+monpen_id+" dan eviden ?",
                icon: "warning",
                buttons:true,
                dangerMode: true,
                showCancelButton: true,
            })
            .then((willDelete) => {
                console.log(willDelete) ;
                if (willDelete){
                    window.location = "/monpen/"+monpen_id+"/delete";
                }
            });
        });
</script>
<script>
     $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable_belitung tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" size="5px"/>' );
    });
 
});
</script>
<!-- ULP -->
<script>
 $(document).ready(function () {
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
 //jalankan function load_data diawal agar data ter-load
  load_data();

 //Iniliasi datepicker pada class input
            $('#filter').click(function () {
                var from_date = $('#from_date').val(); 
                var to_date = $('#to_date').val(); 
                if (from_date != '' && to_date != '') {
                    $('#datatable_ULP').DataTable().destroy();
                    load_data(from_date, to_date);
                } else {
                    alert('Both Date is required');
                }
            });
            $('#refresh').click(function () {
                $('#from_date').val('');
                $('#to_date').val('');
                $('#datatable_ULP').DataTable().destroy();
                load_data();
            });

    function load_data(from_date = '', to_date = '') {
        $('#datatable_ULP').DataTable({
            order: [[14, 'desc'], [15, 'desc']],
            processing:true,
            serverside:true,
            ajax: {
                        url: "{{ route('ajax.get.data.monpen.ULP') }}",
                        type: 'GET',
                        data:{from_date:from_date, to_date:to_date} //jangan lupa kirim parameter tanggal 
                    },
            columns:[
                {data:'UP3',name:'UP3'},
                {data:'ULP',name:'ULP'},
                {data:'penyulang',name:'penyulang'},
                {data:'section',name:'section'},
                {data:'jenis_PMT',name:'jenis_PMT'},
                {data:'indikasi',name:'indikasi'},
                {data:'R',name:'R'},
                {data:'S',name:'S'},
                {data:'T',name:'T'},
                {data:'N',name:'N'},
                {data:'tglpadam',name:'tglpadam'},
                {data:'jampadam',name:'jampadam'},
                {data:'tglnyala',name:'tglnyala'},
                {data:'jamnyala',name:'jamnyala'},
                {data:'durasi',name:'durasi'},
                {data:'beban',name:'beban'},
                {data:'ENS',name:'ENS'},
                {data:'penyebab',name:'penyebab'},
                {data:'kodefgtm',name:'kodefgtm'},
                {data:'vendor',name:'vendor'},
                {data:'jsa',name:'jsa'},
                {data:'pengawas',name:'pengawas'},
                {data:'cuaca',name:'cuaca'},
                {data:'operator',name:'operator'}
            ],
            initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
                } );
                }
                });
                }
            });
        

</script>

<script>
     $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable_ULP tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" size="5px"/>' );
    });
 
});

</script>

<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>
<style>
div.panel-body {
    overflow-x: auto;
    white-space: nowrap;
}
div.panel-body [class*="col"]{  /* TWBS v2 */
    display: inline-block;
    float: none; /* Very important */
}
</style>


<script>
		function copyClipboard() {
		  var elm = document.getElementById("divClipboard");
		  // for Internet Explorer

		  if(document.body.createTextRange) {
			var range = document.body.createTextRange();
			range.moveToElementText(elm);
			range.select();
			document.execCommand("Copy");
			alert("Copied div content to clipboard");
		  }
		  else if(window.getSelection) {
			// other browsers

			var selection = window.getSelection();
			var range = document.createRange();
			range.selectNodeContents(elm);
			selection.removeAllRanges();
			selection.addRange(range);
			document.execCommand("Copy");
			
		  }
		}
		</script> 
<style>
    li {
        cursor: pointer;
}
</style>

<script>
function myFunction(bookURL) {
  window.open(bookURL+'/eviden', "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=700");
}
</script>

@stop



@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">

                <div class="panel-heading">
                  <h3 class="panel-title"><strong>DASHBOARD PER BULAN DI TAHUN </strong></h3>
                </div>


                                    <table class="table table-bordered" id="RM_table">
                                    <thead>
                                        <th>No</th>
                                        <th>ULP</th>
                                        <th>PENYULANG</th>
                                        <th>JAN_PMT</th>
                                        <th>JAN_SEC</th>
                                        <th>FEB_PMT</th>
                                        <th>FEB_SEC</th>
                                        <th>MAR_PMT</th>
                                        <th>MAR_SEC</th>
                                        <th>APR_PMT</th>
                                        <th>APR_SEC</th>
                                        <th>MEI_PMT</th>
                                        <th>MEI_SEC</th>
                                        <th>JUN_PMT</th>
                                        <th>JUN_SEC</th>
                                        <th>JUL_PMT</th>
                                        <th>JUL_SEC</th>
                                        <th>AGS_PMT</th>
                                        <th>AGS_SEC</th>
                                        <th>SEP_PMT</th>
                                        <th>SEP_SEC</th>
                                        <th>OKT_PMT</th>
                                        <th>OKT_SEC</th>
                                        <th>NOV_PMT</th>
                                        <th>NOV_SEC</th>
                                        <th>DES_PMT</th>
                                        <th>DES_SEC</th>

                                    </thead>
                                    <tbody id="RM_body"></tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

 <script>
   /*  $(document).ready(function () {

    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            

        $.ajax({
              type: "GET",
              url: '/getPenyulangSumberTotal',
              data: {
                  tahun:tahun
              },
              success: function(data) {
                  console.log(data);


                  
                  
                  
              },
              error: function (error) {
                  console.log(error);
              }
        })
    });
});     */              
    </script> 

 <script>
  $(document).ready(function () {

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              type: "GET",
              url: '/getPenyulangPKP',
              dataType :'json',
                
    

    success: function(data) {
                 //console.log(data);
           $(document).ready(function () {
                $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) > 6) {
                        $(this).css("background-color", "#d6dad5");
                    }
                    
                    });
            });

            $(document).ready(function () {
                $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) <= 6) {
                        $(this).css("background-color", "#ff4343");
                    }
                  
                   });
            });
            $(document).ready(function () {
                    $("#RM_table td:nth-child(4)").each(function () {
                        if (parseInt($(this).text(), 10) <= 2) {
                            $(this).css("background-color", "yellow");
                        }
                    
                    });
                });
                $(document).ready(function () {
                    $("#RM_table td:nth-child(4)").each(function () {
                    if (parseInt($(this).text(), 10) == 0) {
                            $(this).css("background-color", "#8fff82");
                        }
                    });
                });


                 $.each(data, function(i, RM){
                      $('#RM_body').append($("<tr>")
                      .append($("<td>",).append(RM.No))
                      .append($("<td>",).append(RM.ULP_nama))
                      .append($("<td>").append(RM.penyulang_PKP))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam01))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam01))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam02))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam02))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam03))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam03))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam04))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam04))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam05))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam05))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam06))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam06))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam07))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam07))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam08))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam08))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam09))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam09))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam10))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam10))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam11))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam11))
                      .append($("<td>").append(RM.penyulang_PKP_sumber.rekam12))
                      .append($("<td>").append(RM.penyulang_PKP_sebagian.rekam12))
                      
                      );

                 }); 
                
              },
              error: function (error) {
                  console.log(error);
              }

        });
    });
 </script>

<script>
 /* $(document).ready(function () {

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              type: "GET",
              url: '/getPenyulangSumberTotal',
              dataType :'json',
                
    

    success: function(data) {
                 console.log(data);
                
              },
              error: function (error) {
                  console.log(error);
              }

        });
    });*/
 </script>


 <style>
 
 #RM_table_wrapper {
  width: 100%;
}
#RM_table {
  font-family: Verdana, sans-serif;
  color: black;
  border-collapse: collapse;
  border: 1px solid black;
  margin: 10px auto;
  text-align: center;
  width: 60%;
  max-width: 800px;
}
#RM_table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
#RM_table th {
	font-weight: 600;
  padding: 0.5em;
}
#RM_table td, #RM_table th, #RM_table caption {
  padding: 0.5em;
  border: 1px solid black;

}
#RM_table thead, tfoot tr{
  background: #FFDEAD;
  color: black;
}

 </style>



       
       
@stop
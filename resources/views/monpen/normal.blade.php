@extends('layouts.master')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><strong>PENORMALAN PENYULANG</strong></h3>
								</div>
								<div class="panel-body">
                                <div class="row">
                                    <form action="/monpen/{{$monpen->id}}/update" method="POST" name="form1">
                                    {{csrf_field()}}
                                    <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">UP3</label>
                                        @if(auth()->user()->role == 'UIW.BABEL')
                                                <select name=UP3 class="form-control form-control-sm UP3" id="up3_id" required>
                                                @foreach ($up3_list as $cat)
                                                    <option value="{{$cat->idname}}"
                                                        @if($cat->idname == $monpen->UP3)selected @endif>{{$cat->name}}
                                                    </option>
                                                @endforeach
                                            </select>    
                                        @elseif(auth()->user()->role == 'UP3.BANGKA')
                                            <select name=UP3 class="form-control form-control-sm UP3" id="up3_id" required>
                                            <option value="0" disabled="true" selected="true">Pilih UP3</option>
                                            <option value="UP3 BANGKA" @if ($monpen->UP3 == 'UP3 BANGKA') selected @endif>UP3 BANGKA</option> 
                                            </select>                                       </select>   
                                        @elseif(auth()->user()->role == 'UP3.BELITUNG')
                                            <select name=UP3 class="form-control form-control-sm UP3" id="up3_id" required>
                                            <option value="0" disabled="true" selected="true">Pilih UP3</option>
                                            <option value="UP3 BELITUNG" @if ($monpen->UP3 == 'UP3 BELITUNG') selected @endif>UP3 BELITUNG</option>                                        </select>   
                                            </select>  
                                        @endif       
                       
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">ULP</label>
                                        <select name="ULP" class="form-control form-control-sm ULP" id="ulp_id" required>
                                        @foreach ($ulp_list as $cat)
                                            <option value="{{$cat->idulpname}}"
                                                @if($cat->idulpname == $monpen->ULP)
                                                    selected
                                                @endif 
                                            >{{$cat->ulpname}}
                                            </option>
                                        @endforeach
                                    </select>                           

                                    </select>                                   
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">PENYULANG</label>
                                        <select name=penyulang class="form-control form-control-sm penyulangname" id="penyulang_id" required>
                                        @foreach ($penyulang_list as $cat)
                                            <option value="{{$cat->idpenyulangname}}"
                                                @if($cat->idpenyulangname == $monpen->penyulang)
                                                    selected
                                                @endif 
                                            >{{$cat->penyulangname}}
                                            </option>
                                        @endforeach
                                        </select>                           
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">SECTION</label>
                                        <select name=section class="form-control form-control-sm sectionname" id="section_id" required>
                                        @foreach ($section_list as $cat)
                                            <option value="{{$cat->idsectionname}}"
                                                @if($cat->idsectionname == $monpen->section)
                                                    selected
                                                @endif 
                                            >{{$cat->sectionname}}
                                            </option>
                                        @endforeach
                                        </select>    
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="exampleInputEmail1">JENIS PMT</label>
                                        <input name="jenis_PMT" type="text" value="{{$monpen->jenis_PMT}}" class="form-control form-control-sm" id="jenis_PMT" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="exampleInputEmail1">INDIKASI</label>
                                        <select name=indikasi class="form-control form-control-sm" onkeyup="myFunction()" onload="myFunction()" onmousemove="myFunction()" onchange="myFunction()" onclick="myFunction()" id="indikasi">
                                        <option value="OCR" @if ($monpen->indikasi == 'OCR') selected @endif>OCR</option>
                                        <option value="GFR" @if ($monpen->indikasi == 'GFR') selected @endif>GFR</option>
                                        <option value="UFR" @if ($monpen->indikasi == 'UFR') selected @endif>UFR</option>
                                        <option value="HAR" @if ($monpen->indikasi == 'HAR') selected @endif>HAR</option>
                                        <option value="PMD" @if ($monpen->indikasi == 'PMD') selected @endif>PMD</option>
                                        <option value="TIDAK TERBACA" @if ($monpen->indikasi == 'TIDAK TERBACA') selected @endif>TIDAK TERBACA</option>
									</select>                                   
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">R</label>
                                        <input name="R" type="text" value="{{$monpen->R}}" class="form-control form-control-sm" id="R" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">S</label>
                                        <input name="S" type="text" value="{{$monpen->S}}" class="form-control form-control-sm" id="S" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">T</label>
                                        <input name="T" type="text" value="{{$monpen->T}}" class="form-control form-control-sm" id="T" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-1">
                                        <label for="exampleInputEmail1">N</label>
                                        <input name="N" type="text" value="{{$monpen->N}}" class="form-control form-control-sm" id="N" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">TANGGAL PADAM</label>
                                        <input name="tglpadam" type="date" value="{{$monpen->tglpadam}}" class="form-control form-control-sm" id="tglpadam" onchange="calculateTotal()" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">JAM PADAM</label>
                                        <input name="jampadam" type="time" value="{{$monpen->jampadam}}" class="form-control form-control-sm" id="jampadam" onkeyup="calculateTotal()" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">TANGGAL NYALA</label>
                                        <input name="tglnyala" type="date" value="{{$monpen->tglnyala}}" class="form-control form-control-sm" id="tglnyala" onchange="calculateTotal()" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">JAM NYALA</label>
                                        <input name="jamnyala" type="time" value="{{$monpen->jamnyala}}" class="form-control form-control-sm" id="jamnyala" onkeyup="calculateTotal()" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">BEBAN (A) </label>
                                        <input name="beban" type="text" value="{{$monpen->beban}}" class="form-control form-control-sm" id="beban" onkeyup="calculateTotal()" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">DURASI (Menit)</label>
                                        <input name="durasi" type="text" readOnly="readOnly" value="{{$monpen->durasi}}" class="form-control form-control-sm" id="total" aria-describedby="emailHelp">
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">ENS (kWh)</label>
                                        <input name="ENS" type="text" value="{{$monpen->ENS}}" readOnly="readOnly" class="form-control form-control-sm" id="ENS1" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">VENDOR</label>
                                        <input name="vendor" type="text" value="{{$monpen->vendor}}" class="form-control form-control-sm" id="vendor" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">NOMOR JSA</label>
                                        <input name="jsa" type="text" value="{{$monpen->jsa}}" class="form-control form-control-sm" id="jsa" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="exampleInputEmail1">PENGAWAS</label>
                                        <input name="pengawas" type="text" value="{{$monpen->pengawas}}" class="form-control form-control-sm" id="pengawas" aria-describedby="emailHelp">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">PENYEBAB</label>
                                        <input name="penyebab" type="text" value="{{$monpen->penyebab}}" class="form-control form-control-sm" id="penyebab" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">KODE FGTM</label>
                                        <select name=kodefgtm class="form-control form-control-sm" >
                                        <option value="" @if ($monpen->kodefgtm == '') selected @endif></option>
                                        <option value="BELUM DITEMUKAN" @if ($monpen->kodefgtm == 'BELUM DITEMUKAN') selected @endif>BELUM DITEMUKAN</option>
                                        <option value="I-1 KOMPONEN JTM" @if ($monpen->kodefgtm == 'I-1 KOMPONEN JTM') selected @endif>I-1 KOMPONEN JTM</option>
                                        <option value="I-2 PERALATAN JTM" @if ($monpen->kodefgtm == 'I-2 PERALATAN JTM') selected @endif>I-2 PERALATAN JTM</option>
                                        <option value="I-3 TRAFO DLL" @if ($monpen->kodefgtm == 'I-3 TRAFO DLL') selected @endif>I-3 TRAFO DLL</option>
                                        <option value="I-4 TIANG" @if ($monpen->kodefgtm == 'I-4 TIANG') selected @endif>I-4 TIANG</option>
                                        <option value="E-1 POHON" @if ($monpen->kodefgtm == 'E-1 POHON') selected @endif>E-1 POHON</option>
                                        <option value="E-2 BENCANA ALAM" @if ($monpen->kodefgtm == 'E-2 BENCANA ALAM') selected @endif>E-2 BENCANA ALAM</option>
                                        <option value="E-3 PEK. PIHAK III/BINATANG" @if ($monpen->kodefgtm == 'E-3 PEK. PIHAK III/BINATANG') selected @endif>E-3 PEK. PIHAK III/BINATANG</option>
                                        <option value="E-4 LAYANG2/UMBUL2" @if ($monpen->kodefgtm == 'E-4 LAYANG2/UMBUL2') selected @endif>E-4 LAYANG2/UMBUL2</option>
                                    </select>                                   
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">CUACA</label>
                                        <input name="cuaca" type="text" value="{{$monpen->cuaca}}" class="form-control form-control-sm" id="cuaca" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">OPERATOR</label>
                                        <input name="operator" type="text" value="{{$monpen->operator}}" class="form-control form-control-sm" id="operator" aria-describedby="emailHelp" required>
                                    </div>
                                    </div>
                                    <br />
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <a href="/monpen/data" class="btn btn-danger">Batal</a>
                                    </div>
                                </form>
                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 

		
		function calculateTotal() {
		var beban = document.form1.beban.value;
		var jamnyala = document.form1.jamnyala.value;
		var jampadam = document.form1.jampadam.value;
		var tglpadam = document.form1.tglpadam.value;
		var tglnyala = document.form1.tglnyala.value;
		
		var N1 = new Date ("01/01/2007 "+ jamnyala);
		var N2 = new Date ("01/01/2007 "+ jampadam);
		
		var C1 = Date.parse(N1);
		var C2 = Date.parse(N2);
		
		var tgl1 = Date.parse(tglpadam);
		var tgl2 = Date.parse(tglnyala);
		var selisihtgl = (tgl2 - tgl1)/3600000;
		  
		//document.getElementById('durasi').value = durasi;
		
		var S1 = (C1 - C2)/3600000;
		var total = (selisihtgl + S1)*60;
		document.getElementById('total').value = total;
		var B1 = (beban*20*1.732*0.90)
		var ENS1 = Math.round(B1*S1);
		document.getElementById('ENS1').value = ENS1;
		
		
		
		//var ENSTOTAL = ENS1;
		//document.getElementById('ENSTOTAL').value = ENSTOTAL;
			
	}


</script>

<script type="text/javascript">
function myFunction() {
var indikasi = document.getElementById("indikasi").value;

switch(indikasi) {
  case "OCR":
  	document.getElementById("R").disabled = false;
	document.getElementById("S").disabled = false;
    document.getElementById("T").disabled = false;
	document.getElementById("N").disabled = false;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = true;
	document.getElementById("jsa").disabled = true;
    document.getElementById("pengawas").disabled = true;
    document.getElementById("kodefgtm").disabled = false;
    break;
  case "GFR":
  	document.getElementById("R").disabled = false;
	document.getElementById("S").disabled = false;
    document.getElementById("T").disabled = false;
	document.getElementById("N").disabled = false;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = true;
	document.getElementById("jsa").disabled = true;
    document.getElementById("pengawas").disabled = true;
    document.getElementById("kodefgtm").disabled = false;
    break;
  case "UFR":
  	document.getElementById("R").disabled = true;
	document.getElementById("S").disabled = true;
    document.getElementById("T").disabled = true;
	document.getElementById("N").disabled = true;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = true;
	document.getElementById("jsa").disabled = true;
    document.getElementById("pengawas").disabled = true;
    document.getElementById("kodefgtm").disabled = true;
	break;
   case "HAR":
  	document.getElementById("R").disabled = true;
	document.getElementById("S").disabled = true;
    document.getElementById("T").disabled = true;
	document.getElementById("N").disabled = true;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = false;
	document.getElementById("jsa").disabled = false;
    document.getElementById("pengawas").disabled = false;
    document.getElementById("kodefgtm").disabled = true;
	break;
   case "INV":
  	document.getElementById("R").disabled = true;
	document.getElementById("S").disabled = true;
    document.getElementById("T").disabled = true;
	document.getElementById("N").disabled = true;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = false;
	document.getElementById("jsa").disabled = false;
    document.getElementById("pengawas").disabled = false;
    document.getElementById("kodefgtm").disabled = true;
	break;
   case "PMD":
  	document.getElementById("R").disabled = true;
	document.getElementById("S").disabled = true;
    document.getElementById("T").disabled = true;
	document.getElementById("N").disabled = true;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = true;
	document.getElementById("jsa").disabled = true;
    document.getElementById("pengawas").disabled = true;
    document.getElementById("kodefgtm").disabled = true;
	break;
   case "TIDAK TERBACA":
  	document.getElementById("R").disabled = true;
	document.getElementById("S").disabled = true;
    document.getElementById("T").disabled = true;
	document.getElementById("N").disabled = true;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = true;
	document.getElementById("jsa").disabled = true;
    document.getElementById("pengawas").disabled = true;
    document.getElementById("kodefgtm").disabled = false;
    break;
   default:
  	document.getElementById("R").disabled = false;
	document.getElementById("S").disabled = false;
    document.getElementById("T").disabled = false;
	document.getElementById("N").disabled = false;
    document.getElementById("cuaca").disabled = false;
    document.getElementById("vendor").disabled = false;
	document.getElementById("jsa").disabled = false;
    document.getElementById("pengawas").disabled = false;
    document.getElementById("kodefgtm").disabled = false;
}}
</script>

@stop

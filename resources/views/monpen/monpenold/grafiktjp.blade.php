@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">ULP TANJUNG PANDAN</h3>
								</div>
								<div class="panel-body">
                                    <div id="GRAFIK_TJP_Bar"></div>
                                    <div id="GRAFIK_TJP_Pie"></div>
                                    </div>
                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetPenyulangTJP();
                

            },

            ajaxgetPenyulangTJP: function () {
                var urlPath =  '{!!URL::to('getPenyulangTJP')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) {

                            Highcharts.chart('GRAFIK_TJP_Bar', {
                                chart: {
                                        type: 'bar',
                                        width : 980,
                                        height : '100%'
                                    },
                                    title: {
                                        text: 'GRAFIK GANGGUAN PENYULANG ULP TANJUNG PANDAN'
                                    },
                                    xAxis: {
                                        categories: response.penyulang_TJP
                                    },
                                    yAxis: {
                                        min: 0,
                                        allowDecimals: false,
                                        title: {
                                            text: 'Kali Gangguan'
                                        }
                                    },
                                    legend: {
                                        reversed: true
                                    },
                                    plotOptions: {
                                        series: {
                                            stacking: 'normal'
                                        }
                                    },
                                    series: [{
                                        name: 'SUMBER',
                                        data: response.penyulang_TJP_sumber
                                    },{
                                        name: 'SEBAGIAN',
                                        data: response.penyulang_TJP_sebagian
                                    }]
                            });
                                
                                }
                };

                charts.init();

                } )( jQuery );
    </script>
       
       
    <script>    
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetKodeTJP();
                

            },

            ajaxgetKodeTJP: function () {
                var urlPath =  '{!!URL::to('getKodeTJP')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) 
                {
                    // Radialize the colors
                    Highcharts.setOptions({
                        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                            return {
                                radialGradient: {
                                    cx: 0.5,
                                    cy: 0.3,
                                    r: 0.7
                                },
                                stops: [
                                    [0, color],
                                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                                ]
                            };
                        })
                    });

                    // Build the chart
                    Highcharts.chart('GRAFIK_TJP_Pie', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: 'PRESENTASE PENYEBAB GANGGUAN ULP TANJUNG PANDAN'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: ({point.y}) {point.percentage:.1f} %',
                                    connectorColor: 'silver'
                                }
                            }
                        },
                        series: [{
                            name: '%',
                            data: [
                                { name: response.kode_T[0] , y: response.kode_TJP[0] },
                                { name: response.kode_T[1] , y: response.kode_TJP[1] },
                                { name: response.kode_T[2] , y: response.kode_TJP[2] },
                                { name: response.kode_T[3] , y: response.kode_TJP[3] },
                                { name: response.kode_T[4] , y: response.kode_TJP[4] },
                                { name: response.kode_T[5] , y: response.kode_TJP[5] },
                                { name: response.kode_T[6] , y: response.kode_TJP[6] },
                                { name: response.kode_T[7] , y: response.kode_TJP[7] },
                                { name: response.kode_T[8] , y: response.kode_TJP[8] },

                            ]
                        }]
                    });
                }
            };

                charts.init();

                } )( jQuery );


    </script>
                <style type="text/css">
                .highcharts-data-table table {
                    font-family: Verdana, sans-serif;
                    border-collapse: collapse;
                    border: 1px solid #EBEBEB;
                    margin: 10px auto;
                    text-align: center;
                    width: 100%;
                    max-width: 500px;
                }
                .highcharts-data-table caption {
                    padding: 1em 0;
                    font-size: 1.2em;
                    color: #555;
                }
                .highcharts-data-table th {
                    font-weight: 600;
                    padding: 0.5em;
                }
                .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
                    padding: 0.5em;
                }
                .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
                    background: #f8f8f8;
                }
                .highcharts-data-table tr:hover {
                    background: #f1f7ff;
                }

                </style>
@stop
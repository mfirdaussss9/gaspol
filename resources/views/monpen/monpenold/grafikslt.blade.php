@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">ULP SUNGAILIAT</h3>
								</div>
								<div class="panel-body">
                                <form action="" method="GET">
                                {{csrf_field()}}
                                    <input type="date" name="from" value="">
                                    <input type="date" name="to" value="">
                                    <input type="submit" value="CARI">
                                </form>
                               
                                    <div id="GRAFIK_SLT_Bar"></div>
                                    <div id="pie"></div>
                                    </div>
                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>


    <script>
        ( function ( $ ) {

        var charts = {
            
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetPenyulangSLT();
                

            },

            ajaxgetPenyulangSLT: function () {
                var urlPath =  '{!!URL::to('getPenyulangSLT')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) {

                var chart;

                var sortData = function(chartSource) {
                    
                    console.time('sorting');
                    var series = chartSource.series;
                    var axis = chartSource.xAxis[0];
                    var categories = [];
                    if ($.isArray(series)) {
                    var ser = $.grep(series, function(ser, seriesIndex) {
                        return ser.visible;
                    })[0];
                    $.each(ser.data, function(dataIndex, datum) {
                        var obj = {
                        name: datum.category,
                        index: dataIndex,
                        stackTotal: datum.stackTotal
                        }
                        categories.push(obj);
                    });
                    }
                    categories.sort(function(a, b) {
                    var aName = a.name.toLowerCase();
                    var bName = b.name.toLowerCase();
                    var aTotal = a.stackTotal;
                    var bTotal = b.stackTotal;
                    if (aTotal === bTotal) {
                        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                    } else {
                        return ((aTotal > bTotal) ? -1 : ((aTotal < bTotal) ? 1 : 0));
                    }
                    });
                    var mappedIndex = $.map(categories, function(category, index) {
                    return category.index;
                    });
                    categories = $.map(categories, function(category, index) {
                    return category.name;
                    });
                    // console.log(categories);
                    //console.log(mappedIndex);
                    axis.setCategories(categories);
                    $.each(series, function(seriesIndex, ser) {
                    var data = $.map(mappedIndex, function(mappedIndex, origIndex) {
                        return ser.data[mappedIndex].y;
                    });
                    ser.setData(data, false);
                    });
                    chartSource.redraw();
                    console.timeEnd('sorting');


                };

                                $(document).ready(function() {
                                    chart = new Highcharts.Chart({
                                    chart: {
                                        renderTo: 'GRAFIK_SLT_Bar',
                                        type: 'bar',
                                        width : 980,
                                        height : '60%'
                                    },
                                    title: {
                                        text: 'GRAFIK GANGGUAN PENYULANG ULP SUNGAILIAT'
                                    },
                                    xAxis: {
                                        categories: response.penyulang
                                    },
                                    yAxis: {
                                        min: 0,
                                        allowDecimals: false,
                                        title: {
                                            text: 'Kali Gangguan'
                                        }
                                    },
                                    legend: {
                                        reversed: true
                                    },

                                    plotOptions: {
                                        series: {
                                            stacking: 'normal',
                                            events: {
                                                    hide: function() {
                                                    sortData(chart);
                                                    },
                                                    show: function() {
                                                    sortData(chart);
                                                    },
                                                    
                                                }
                                        }
                                    },
                                    series: [{
                                        name: 'SECTION',
                                        data: response.penyulang_SLT_sebagian,
                                        zones: [{
                                            value: 1,
                                            color: 'green',
                                        }, {
                                            value: 3,
                                            color: 'red',
                                        }, {
                                            value: 7,
                                            color: 'red',
                                        }, {
                                            color: 'black',
                                        }]
                                    },{
                                       
                                        name: 'PMT',
                                        data: response.penyulang_SLT_sumber,
                                        zones: [{
                                            value: 1,
                                            color: 'green',
                                        }, {
                                            value: 3,
                                            color: 'blue',
                                        }, {
                                            value: 7,
                                            color: 'red',
                                        }, {
                                            color: 'black',
                                        }]
                                    }],
                                
                                        exporting: {
                                                csv: {
                                                    columnHeaderFormatter: function(item, key) {
                                                        if (!item || item instanceof Highcharts.Axis) {
                                                            return 'PENYULANG'
                                                        } else {
                                                            return item.name;
                                                        }
                                                    }
                                                }
                                            },
                                    }
                                    
                                    , function(chart) {
                                        sortData(chart);
                                        });

                                   
                                    });   

                                
                                }
                };

                charts.init();

                } )( jQuery );
    </script>
<script>
                            </script>
    <script>    
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetKodeSLT();
                

            },

            ajaxgetKodeSLT: function () {
                var urlPath =  '{!!URL::to('getKodeSLT')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) 
                {
                    // Radialize the colors
                    Highcharts.setOptions({
                        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                            return {
                                radialGradient: {
                                    cx: 0.5,
                                    cy: 0.3,
                                    r: 0.7
                                },
                                stops: [
                                    [0, color],
                                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                                ]
                            };
                        })
                    });

                    // Build the chart
                    Highcharts.chart('pie', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: 'PRESENTASE PENYEBAB GANGGUAN ULP SUNGAILIAT'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: ({point.y}) {point.percentage:.1f} %',
                                    connectorColor: 'silver'
                                }
                            }
                        },
                        series: [{
                            name: '%',
                            data: [
                                { name: response.kode[0] , y: response.kode_SLT[0] },
                                { name: response.kode[1] , y: response.kode_SLT[1] },
                                { name: response.kode[2] , y: response.kode_SLT[2] },
                                { name: response.kode[3] , y: response.kode_SLT[3] },
                                { name: response.kode[4] , y: response.kode_SLT[4] },
                                { name: response.kode[5] , y: response.kode_SLT[5] },
                                { name: response.kode[6] , y: response.kode_SLT[6] },
                                { name: response.kode[7] , y: response.kode_SLT[7] },
                                { name: response.kode[8] , y: response.kode_SLT[8] },

                            ]
                        }],
                         
                    });
                }
            };

                charts.init();

                } )( jQuery );


    </script>
                <style type="text/css">
                .highcharts-data-table table {
                    font-family: Verdana, sans-serif;
                    border-collapse: collapse;
                    border: 1px solid #EBEBEB;
                    margin: 10px auto;
                    text-align: center;
                    width: 100%;
                    max-width: 500px;
                }
                .highcharts-data-table caption {
                    padding: 1em 0;
                    font-size: 1.2em;
                    color: #555;
                }
                .highcharts-data-table th {
                    font-weight: 600;
                    padding: 0.5em;
                }
                .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
                    padding: 0.5em;
                }
                .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
                    background: #f8f8f8;
                }
                .highcharts-data-table tr:hover {
                    background: #f1f7ff;
                }

                </style>
@stop

$(document).ready(function () {
                                    $("#highcharts-data-table-0 td:nth-child(2)").each(function () {
                                        if (parseInt($(this).text(), 10) == 0) {
                                            $(this).parent("tr").css("background-color", "red");
                                        }
                                    });
                                });

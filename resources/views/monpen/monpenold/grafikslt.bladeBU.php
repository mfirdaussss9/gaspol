@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">ULP SUNGAILIAT</h3>
								</div>
								<div class="panel-body">
                                    <div id="GRAFIK_SLT_Bar"></div>
                                    <div id="pie"></div>

                                   
                                    </div>

                                </div>
								</div>
							</div>

                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetPenyulangSLT();
                

            },

            ajaxgetPenyulangSLT: function () {
                var urlPath =  '{!!URL::to('getPenyulangSLT')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) {

                            Highcharts.chart('GRAFIK_SLT_Bar', {
                                chart: {
                                        type: 'bar',
                                        width : 980 
                                    },
                                    title: {
                                        text: 'GRAFIK GANGGUAN PENYULANG ULP SUNGAILIAT'
                                    },
                                    xAxis: {
                                        categories: response.penyulang
                                    },
                                    yAxis: {
                                        min: 0,
                                        allowDecimals: false,
                                        title: {
                                            text: 'Kali Gangguan'
                                        }
                                    },
                                    legend: {
                                        reversed: true
                                    },
                                    plotOptions: {
                                        series: {
                                            stacking: 'normal'
                                        }
                                    },
                                    series: [{
                                        name: 'SUMBER',
                                        data: response.penyulang_SLT_sumber
                                    },{
                                        name: 'SEBAGIAN',
                                        data: response.penyulang_SLT_sebagian
                                    }]
                            });
                                
                                }
                };

                charts.init();

                } )( jQuery );
    </script>
       
       
    <script>    
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetKodeSLT();
                

            },

            ajaxgetKodeSLT: function () {
                var urlPath =  '{!!URL::to('getKodeSLT')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) 
                {
                    // Radialize the colors
                    Highcharts.setOptions({
                        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                            return {
                                radialGradient: {
                                    cx: 0.5,
                                    cy: 0.3,
                                    r: 0.7
                                },
                                stops: [
                                    [0, color],
                                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                                ]
                            };
                        })
                    });

                    // Build the chart
                    Highcharts.chart('pie', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: 'PRESENTASE PENYEBAB GANGGUAN ULP SUNGAILIAT'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: ({point.y}) {point.percentage:.1f} %',
                                    connectorColor: 'silver'
                                }
                            }
                        },
                        series: [{
                            name: '%',
                            data: [
                                { name: response.kode[0] , y: response.kode_SLT[0] },
                                { name: response.kode[1] , y: response.kode_SLT[1] },
                                { name: response.kode[2] , y: response.kode_SLT[2] },
                                { name: response.kode[3] , y: response.kode_SLT[3] },
                                { name: response.kode[4] , y: response.kode_SLT[4] },
                                { name: response.kode[5] , y: response.kode_SLT[5] },
                                { name: response.kode[6] , y: response.kode_SLT[6] },
                                { name: response.kode[7] , y: response.kode_SLT[7] },
                                { name: response.kode[8] , y: response.kode_SLT[8] },

                            ]
                        }]
                    });
                }
            };

                charts.init();

                } )( jQuery );


    </script>
                <style type="text/css">
                .highcharts-data-table table {
                    font-family: Verdana, sans-serif;
                    border-collapse: collapse;
                    border: 1px solid #EBEBEB;
                    margin: 10px auto;
                    text-align: center;
                    width: 100%;
                    max-width: 500px;
                }
                .highcharts-data-table caption {
                    padding: 1em 0;
                    font-size: 1.2em;
                    color: #555;
                }
                .highcharts-data-table th {
                    font-weight: 600;
                    padding: 0.5em;
                }
                .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
                    padding: 0.5em;
                }
                .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
                    background: #f8f8f8;
                }
                .highcharts-data-table tr:hover {
                    background: #f1f7ff;
                }

                </style>
@stop
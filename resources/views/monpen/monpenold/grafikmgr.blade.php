@extends('layouts.master')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">ULP MANGGAR</h3>
								</div>
								<div class="panel-body">
                                    <div id="GRAFIK_MGR_Bar"></div>
                                    <div id="GRAFIK_MGR_Pie"></div>
                                    </div>
                                </div>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetPenyulangMGR();
                

            },

            ajaxgetPenyulangMGR: function () {
                var urlPath =  '{!!URL::to('getPenyulangMGR')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) {

                            Highcharts.chart('GRAFIK_MGR_Bar', {
                                chart: {
                                        type: 'bar',
                                        width : 980,
                                        height : '100%'
                                    },
                                    title: {
                                        text: 'GRAFIK GANGGUAN PENYULANG ULP MANGGAR'
                                    },
                                    xAxis: {
                                        categories: response.penyulang_MGR
                                    },
                                    yAxis: {
                                        min: 0,
                                        allowDecimals: false,
                                        title: {
                                            text: 'Kali Gangguan'
                                        }
                                    },
                                    legend: {
                                        reversed: true
                                    },
                                    plotOptions: {
                                        series: {
                                            stacking: 'normal'
                                        }
                                    },
                                    series: [{
                                        name: 'SUMBER',
                                        data: response.penyulang_MGR_sumber
                                    },{
                                        name: 'SEBAGIAN',
                                        data: response.penyulang_MGR_sebagian
                                    }]
                            });
                                
                                }
                };

                charts.init();

                } )( jQuery );
    </script>
       
       
    <script>    
        ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxgetKodeMGR();
                

            },

            ajaxgetKodeMGR: function () {
                var urlPath =  '{!!URL::to('getKodeMGR')!!}';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    //console.log (response);
                    charts.createCompletedJobsChart ( response );
                });
            },

            /**
             * Created the Completed Jobs Chart
             */
            createCompletedJobsChart: function ( response ) 
                {
                    // Radialize the colors
                    Highcharts.setOptions({
                        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                            return {
                                radialGradient: {
                                    cx: 0.5,
                                    cy: 0.3,
                                    r: 0.7
                                },
                                stops: [
                                    [0, color],
                                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                                ]
                            };
                        })
                    });

                    // Build the chart
                    Highcharts.chart('GRAFIK_MGR_Pie', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: 'PRESENTASE PENYEBAB GANGGUAN ULP MANGGAR'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: ({point.y}) {point.percentage:.1f} %',
                                    connectorColor: 'silver'
                                }
                            }
                        },
                        series: [{
                            name: '%',
                            data: [
                                { name: response.kode_MG[0] , y: response.kode_MGR[0] },
                                { name: response.kode_MG[1] , y: response.kode_MGR[1] },
                                { name: response.kode_MG[2] , y: response.kode_MGR[2] },
                                { name: response.kode_MG[3] , y: response.kode_MGR[3] },
                                { name: response.kode_MG[4] , y: response.kode_MGR[4] },
                                { name: response.kode_MG[5] , y: response.kode_MGR[5] },
                                { name: response.kode_MG[6] , y: response.kode_MGR[6] },
                                { name: response.kode_MG[7] , y: response.kode_MGR[7] },
                                { name: response.kode_MG[8] , y: response.kode_MGR[8] },

                            ]
                        }]
                    });
                }
            };

                charts.init();

                } )( jQuery );


    </script>
                <style type="text/css">
                .highcharts-data-table table {
                    font-family: Verdana, sans-serif;
                    border-collapse: collapse;
                    border: 1px solid #EBEBEB;
                    margin: 10px auto;
                    text-align: center;
                    width: 100%;
                    max-width: 500px;
                }
                .highcharts-data-table caption {
                    padding: 1em 0;
                    font-size: 1.2em;
                    color: #555;
                }
                .highcharts-data-table th {
                    font-weight: 600;
                    padding: 0.5em;
                }
                .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
                    padding: 0.5em;
                }
                .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
                    background: #f8f8f8;
                }
                .highcharts-data-table tr:hover {
                    background: #f1f7ff;
                }

                </style>
@stop
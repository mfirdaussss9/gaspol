@extends('layouts.master')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                  <h3 class="panel-title"><strong>DASHBOARD</strong></h3>
                  <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                  </li>
                </ul>
                  <div class="right">
                  <form action="" method="get" class="form-inline">
                  <div class="form-group mb-2">
                  <select name=tahun class="form-control form-control-sm">
                      <option value="0" disabled="true" selected="true">Pilih Tahun</option>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
									</select>    
                  </div>
                  <input type="submit" value="CARI" class="btn btn-md btn-primary">
                  <a href="/monpen/dashboard" class="btn btn-warning btn-md">ULANG</a>
                  </form>
                  </div>
                
                </div>
                      <div class="panel-body">
                                    <div id="container"></div>
                                </div>  
                              </div>  
                           </form>
                                
                      </div>
							    </div>
                </div>
            </div>
        </div>
      </div>
</div>

<script>
  Highcharts.chart('container', {

chart: {
  type: 'column'
},

title: {
  text: 'GRAFIK GANGGUAN UP3 BANGKA'
},

xAxis: {
  categories: ['Jan-{{$tahun}}', 'Feb-{{$tahun}}', 'Mar-{{$tahun}}', 'Apr-{{$tahun}}', 'Mei-{{$tahun}}','Jun-{{$tahun}}','Jul-{{$tahun}}','Ags-{{$tahun}}','Sep-{{$tahun}}','Okt-{{$tahun}}','Nov-{{$tahun}}','Des-{{$tahun}}']
},

yAxis: {
  allowDecimals: false,
  min: 0,
  title: {
    text: 'kali gangguan'
  },
  stackLabels: {
      enabled: true,
      style: {
        fontWeight: 'bold',
        color: ( // theme
          Highcharts.defaultOptions.title.style &&
          Highcharts.defaultOptions.title.style.color
        ) || 'gray'
      }
    }
},

legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: false,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },

tooltip: {
  formatter: function () {
    return '<b>' + this.x + '</b><br/>' +
      this.series.name + ': ' + this.y + '<br/>' +
      'Total: ' + this.point.stackTotal;
  }
},

plotOptions: {
  column: {
    stacking: 'normal'
  }
},

series: [{
  name: 'PKP-PMT',
  data: [
  {{$janpkp_p}}, 
  {{$febpkp_p}}, 
  {{$marpkp_p}}, 
  {{$aprpkp_p}}, 
  {{$meipkp_p}}, 
  {{$junpkp_p}}, 
  {{$julpkp_p}}, 
  {{$agspkp_p}}, 
  {{$seppkp_p}}, 
  {{$oktpkp_p}}, 
  {{$novpkp_p}}, 
  {{$despkp_p}}
  ],
  stack: 'PKP',
}, {
  name: 'PKP-SEC',
  data: [
  {{$janpkp_s}}, 
  {{$febpkp_s}}, 
  {{$marpkp_s}}, 
  {{$aprpkp_s}}, 
  {{$meipkp_s}}, 
  {{$junpkp_s}}, 
  {{$julpkp_s}}, 
  {{$agspkp_s}}, 
  {{$seppkp_s}}, 
  {{$oktpkp_s}}, 
  {{$novpkp_s}}, 
  {{$despkp_s}}
  ],
  stack: 'PKP',
}, {
  name: 'SLT-PMT',
  data: [
  {{$janslt_p}}, 
  {{$febslt_p}}, 
  {{$marslt_p}}, 
  {{$aprslt_p}}, 
  {{$meislt_p}}, 
  {{$junslt_p}}, 
  {{$julslt_p}}, 
  {{$agsslt_p}}, 
  {{$sepslt_p}}, 
  {{$oktslt_p}}, 
  {{$novslt_p}}, 
  {{$desslt_p}}
  ],
  stack: 'SLT'
}, {
  name: 'SLT-SEC',
  data: [
  {{$janslt_s}}, 
  {{$febslt_s}}, 
  {{$marslt_s}}, 
  {{$aprslt_s}}, 
  {{$meislt_s}}, 
  {{$junslt_s}}, 
  {{$julslt_s}}, 
  {{$agsslt_s}}, 
  {{$sepslt_s}}, 
  {{$oktslt_s}}, 
  {{$novslt_s}}, 
  {{$desslt_s}}
  ],
  stack: 'SLT'
}, {
  name: 'MTK-PMT',
  data: [
  {{$janmtk_p}}, 
  {{$febmtk_p}}, 
  {{$marmtk_p}}, 
  {{$aprmtk_p}}, 
  {{$meimtk_p}}, 
  {{$junmtk_p}}, 
  {{$julmtk_p}}, 
  {{$agsmtk_p}}, 
  {{$sepmtk_p}}, 
  {{$oktmtk_p}}, 
  {{$novmtk_p}}, 
  {{$desmtk_p}}
  ],
  stack: 'MTK'
}, {
  name: 'MTK-SEC',
  data: [
  {{$janmtk_s}}, 
  {{$febmtk_s}}, 
  {{$marmtk_s}}, 
  {{$aprmtk_s}}, 
  {{$meimtk_s}}, 
  {{$junmtk_s}}, 
  {{$julmtk_s}}, 
  {{$agsmtk_s}}, 
  {{$sepmtk_s}}, 
  {{$oktmtk_s}}, 
  {{$novmtk_s}}, 
  {{$desmtk_s}}
  ],
  stack: 'MTK'
}, {
  name: 'TBL-PMT',
  data: [
  {{$jantbl_p}}, 
  {{$febtbl_p}}, 
  {{$martbl_p}}, 
  {{$aprtbl_p}}, 
  {{$meitbl_p}}, 
  {{$juntbl_p}}, 
  {{$jultbl_p}}, 
  {{$agstbl_p}}, 
  {{$septbl_p}}, 
  {{$okttbl_p}}, 
  {{$novtbl_p}}, 
  {{$destbl_p}}
  ],
  stack: 'TBL'
}, {
  name: 'TBL-SEC',
  data: [
  {{$jantbl_s}}, 
  {{$febtbl_s}}, 
  {{$martbl_s}}, 
  {{$aprtbl_s}}, 
  {{$meitbl_s}}, 
  {{$juntbl_s}}, 
  {{$jultbl_s}}, 
  {{$agstbl_s}}, 
  {{$septbl_s}}, 
  {{$okttbl_s}}, 
  {{$novtbl_s}}, 
  {{$destbl_s}}
  ],
  stack: 'TBL'
}, {
  name: 'KBA-PMT',
  data: [
  {{$jankba_p}}, 
  {{$febkba_p}}, 
  {{$markba_p}}, 
  {{$aprkba_p}}, 
  {{$meikba_p}}, 
  {{$junkba_p}}, 
  {{$julkba_p}}, 
  {{$agskba_p}}, 
  {{$sepkba_p}}, 
  {{$oktkba_p}}, 
  {{$novkba_p}}, 
  {{$deskba_p}}
  ],
  stack: 'KBA'
}, {
  name: 'KBA-SEC',
  data: [
  {{$jankba_p}}, 
  {{$febkba_p}}, 
  {{$markba_p}}, 
  {{$aprkba_p}}, 
  {{$meikba_p}}, 
  {{$junkba_p}}, 
  {{$julkba_p}}, 
  {{$agskba_p}}, 
  {{$sepkba_p}}, 
  {{$oktkba_p}}, 
  {{$novkba_p}}, 
  {{$deskba_p}}
  ],
  stack: 'KBA'
}, {
  name: 'BGK-PMT',
  data: [
  {{$janbgk_p}}, 
  {{$febbgk_p}}, 
  {{$marbgk_p}}, 
  {{$aprbgk_p}}, 
  {{$meibgk_p}}, 
  {{$junbgk_p}}, 
  {{$julbgk_p}}, 
  {{$agsbgk_p}}, 
  {{$sepbgk_p}}, 
  {{$oktbgk_p}}, 
  {{$novbgk_p}}, 
  {{$desbgk_p}}
  ],

  stack: 'BGK'
}, {
  name: 'BGK-SEC',
  data: [
  {{$janbgk_s}}, 
  {{$febbgk_s}}, 
  {{$marbgk_s}}, 
  {{$aprbgk_s}}, 
  {{$meibgk_s}}, 
  {{$junbgk_s}}, 
  {{$julbgk_s}}, 
  {{$agsbgk_s}}, 
  {{$sepbgk_s}}, 
  {{$oktbgk_s}}, 
  {{$novbgk_s}}, 
  {{$desbgk_s}}
  ],
  stack: 'BGK'

}]
});

</script>

<style>
  .highcharts-figure, .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  margin: 1em auto;
}

#container {
  height: 100%;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>

@stop



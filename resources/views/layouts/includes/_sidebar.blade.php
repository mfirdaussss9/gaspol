<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="{{asset('admin/assets/scripts/klorofil-common.js')}}"></script>



<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="fa fa-user"></i> <span class="badge badge-danger" style="font-size: 13pt">{{Auth::user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
								<li><a href="/monpen/changepassword" class="{{ (request()->is('monpen/changepassword*')) ? 'active' : '' }}">Ganti Password</a></li>
								</ul>
							</div>
						</li>
						<li><a href="/monpen/dashboard" class="{{ (request()->is('monpen/dashboard*')) ? 'active' : '' }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<li><a href="/monpen/data" class="{{ (request()->is('monpen/data*')) ? 'active' : '' }}" ><i class="fa fa-database"></i> <span>Database</span></a></li>
						<!--<li><a href="/monpen/grafik" class="{{ (request()->is('monpen/grafik*')) ? 'active' : '' }}" ><i class="fa fa-database"></i> <span>grafik</span></a></li>
						<li><a href="/monpen/sltdetil" class="{{ (request()->is('monpen/sltdetil*')) ? 'active' : '' }}" ><i class="fa fa-database"></i> <span>sltdetil</span></a></li>-->

						@if(auth()->user()->role == 'UP3.BANGKA' )
							<li><a href="/monpen/kondisipkp" class="{{ (request()->is('monpen/kondisipkp*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Pangkalpinang</span></a></li>
							<li><a href="/monpen/kondisislt" class="{{ (request()->is('monpen/kondisislt*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Sungailiat</span></a></li>
							<li><a href="/monpen/kondisimtk" class="{{ (request()->is('monpen/kondisimtk*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Mentok</span></a></li>
							<li><a href="/monpen/kondisitbl" class="{{ (request()->is('monpen/kondisitbl*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Toboali</span></a></li>
							<li><a href="/monpen/kondisikba" class="{{ (request()->is('monpen/kondisikba*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Koba</span></a></li>
						@elseif(auth()->user()->role == 'UP3.BELITUNG')
						<li><a href="/monpen/kondisitjp" class="{{ (request()->is('monpen/kondisitjp*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Tanjung Pandan</span></a></li>
						<li><a href="/monpen/kondisimgr" class="{{ (request()->is('monpen/kondisimgr*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Manggar</span></a></li>
						@elseif(auth()->user()->role = 'UIW.BABEL' && 'ULP' )
							<li><a href="/monpen/kondisislt" class="{{ (request()->is('monpen/kondisislt*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Sungailiat</span></a></li>
							<li><a href="/monpen/slt" class="{{ (request()->is('monpen/slt*')) ? 'active' : '' }}" ><i class="fa fa-bolt"></i> <span>REKAPAN GANGGUAN ULP SUNGAILIAT2020</span></a></li>
							<li><a href="/monpen/slt2021" class="{{ (request()->is('monpen/slt2021*')) ? 'active' : '' }}" ><i class="fa fa-bolt"></i> <span>REKAPAN GANGGUAN ULP SUNGAILIAT2021</span></a></li>

							<!--<li><a href="/monpen/kondisipkp" class="{{ (request()->is('monpen/kondisipkp*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Pangkalpinang</span></a></li>
							<li><a href="/monpen/kondisimtk" class="{{ (request()->is('monpen/kondisimtk*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Mentok</span></a></li>
							<li><a href="/monpen/kondisitbl" class="{{ (request()->is('monpen/kondisitbl*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Toboali</span></a></li>
							<li><a href="/monpen/kondisikba" class="{{ (request()->is('monpen/kondisikba*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Koba</span></a></li>
							<li><a href="/monpen/kondisitjp" class="{{ (request()->is('monpen/kondisitjp*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Tanjung Pandan</span></a></li>
							<li><a href="/monpen/kondisimgr" class="{{ (request()->is('monpen/kondisimgr*')) ? 'active' : '' }}"><i class="fa fa-bolt"></i><span>ULP Manggar</span></a></li> -->
						@endif
						
						<li><a href="/logout" class=""><i class="lnr lnr-exit"></i> <span>Log Out</span></a></li>
						
					</ul>
				</nav>
			</div>
		</div>
		
